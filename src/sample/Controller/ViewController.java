package sample.Controller;

import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.View.*;

import java.util.ArrayList;

public class ViewController {

    static Stage stage;
    static int WIDTH = 750;
    static int HEIGHT = 750;

    public static void setStage(Stage stage){
        ViewController.stage = stage;
    }

    public static void goToHomeMenu() {
        HomeMenuView homeMenuView = new HomeMenuView(WIDTH, HEIGHT);
        stage.setScene(homeMenuView.getScene());
        stage.setTitle("HOME MENU");
    }

    public static void goToUserMenu() {
        UserMenuView userMenuView = new UserMenuView(WIDTH, HEIGHT);
        stage.setScene(userMenuView.getScene());
        stage.setTitle("USER MENU");
    }

    public static void goToTechMenu() {
        TechMenuView techMenuView = new TechMenuView(WIDTH, HEIGHT);
        stage.setScene(techMenuView.getScene());
        stage.setTitle("TECH MENU");
    }

    public static void goToUserRegister() {
        UserRegisterView userRegisterView = new UserRegisterView(WIDTH, HEIGHT);
        stage.setScene(userRegisterView.getScene());
        stage.setTitle("USER MENU");
    }

    // redirects to user page : anonyme or reloader.
    public static void goToUserScooterRentingPage(int id) {
        // check if user is anonyme or reloader
        Scene userScene;
        if (DbController.isReloader(id)){
            userScene = new UserScooterRentingPageView(id, true, WIDTH, HEIGHT).getScene();
            stage.setTitle("RELOADER USER PAGE");
        } else {
            userScene = new UserScooterRentingPageView(id, false, WIDTH, HEIGHT).getScene();
            stage.setTitle("USER PAGE");
        }
        stage.setScene(userScene);
    }




    public static void goToUserActivityPage(int userID){

        // userID, scooterID, dateDébut, prix
        ArrayList<Object> tripData = DbController.getDataFromCurrentRent(userID);

        // scooterID, initialLoadd, startTime
        ArrayList<Object> reloadData = null;
        if (DbController.isReloader(userID)) {
            reloadData = DbController.getDataFromCurrentReload(userID);
        }
        UserActivityPageView userActivityPageView = new UserActivityPageView(userID, tripData, reloadData, WIDTH, HEIGHT);
        stage.setScene(userActivityPageView.getScene());
        stage.setTitle("USER ACTIVITY PAGE");
    }



    public static void goToTechUserPage(String id) {
        TechUserPageView techPageUserView = new TechUserPageView(id, WIDTH, HEIGHT);
        stage.setScene(techPageUserView.getScene());
        stage.setTitle("TECH USER PAGE");
    }

    public static void goToTechScooterPage(String id) {
        TechScooterPageView techScooterPageView = new TechScooterPageView(id, WIDTH, HEIGHT);
        stage.setScene(techScooterPageView.getScene());
        stage.setTitle("TECH SCOOTER PAGE");
    }

    public static void goToTechDecisionPage(String techId){
        TechDecisionPageView techDecisionPageView = new TechDecisionPageView(techId, WIDTH, HEIGHT);
        stage.setScene(techDecisionPageView.getScene());
        stage.setTitle("TECH DECISION PAGE");
    }

}
