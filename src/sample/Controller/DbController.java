package sample.Controller;

import sample.Model.*;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DbController {

    public static ArrayList<Scooter> getAvailableScooters() throws SQLException {

        ArrayList<Scooter> scooters = DbAccess.getAvailableScooters();

        return scooters;

    }

    public static ArrayList<Scooter> getScooters() throws SQLException {

        ArrayList<Scooter> scooters = DbAccess.getScooters();

        return scooters;

    }

    public static int registerAnonymeUser(String password, String bankAccount) {
        

        return DbAccess.addAnonymeUser(password, bankAccount);

        
        
    }
    public static boolean hasUnresolvedComplaint(int scooterID){
        return DbAccess.hasUnresolvedComplaint(scooterID);
    }

    public static boolean isUnload(int scooterID){
        return DbAccess.isUnload(scooterID);
    }

    public static boolean addPlaint(int scooterID, int userID) {
        return DbAccess.addPlaint(scooterID, userID);
    }

    public static boolean checkIdMatchPassword(int id, String password) {
        return DbAccess.checkIdMatchPasswordUser(id, password);
    }

    public static void rentScooter(int selectedScooterID, int userID) {
        DbAccess.rentScooter(selectedScooterID);
        DbAccess.startTrip(selectedScooterID, userID);
    }

    public static boolean returnScooter(int selectedScooterID, int userID){

        if (DbAccess.endTrip(selectedScooterID, userID)){
            DbAccess.returnScooter(selectedScooterID, 0);
            return true;
        } else {
            return false;
        }
    }

    public static boolean returnScooterFromReload(int selectedScooterID, int userID){

        if (DbAccess.endReload(selectedScooterID, userID)){
            DbAccess.returnScooter(selectedScooterID, 4);
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList<Object> getDataFromCurrentRent(int userId) {

        return DbAccess.getDataFromCurrentRent(userId);

    }

    public static void addScooter(int id, String model) {
        Date date = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyy-MM-dd'T'hh:mm:ss");
        DbAccess.addScooter(id, 4, formater.format(date), model, false);
    }

    public static void changeScooterStatus(int selectedScooterID) {
        DbAccess.changeScooterStatus(selectedScooterID);
    }

    public static int getScooterStatus(int scooterID){
        return DbAccess.getScooterStatus(scooterID);
    }


    public static boolean takeScooterForReload(int scooterID, int userID) {

        if (DbAccess.getScooterLoad(scooterID) == 4){
            return false;
        } else {
            DbAccess.rentScooter(scooterID);
            DbAccess.startReload(scooterID, userID);
            return true;
        }

    }

    public static boolean checkScooterExists(int id) throws SQLException {
        return DbAccess.checkScooterExists(id);
    }

    public static void deleteScooter(int selectedScooterID) throws SQLException {
        DbAccess.deleteScooter(selectedScooterID);
    }

    public static void treatPlaint(int scooterID, String techID, String note) {
        DbAccess.treatPlaint(scooterID, techID, note);
    }

    public static boolean checkIdMatchPasswordMechanic(String id, String password){
        return DbAccess.checkIdMatchPasswordMechanic(id, password);
    }

    public static int registerReloader(String firstname, String surname, String password, String phone,
                                       String adNum, String adCity, String adCP, String adStreet,
                                       String bankaccount) {
        return DbAccess.addReloader(surname, firstname, password, phone, adCity, adCP, adStreet,
                adNum, bankaccount);
    }

    public static ArrayList<Reparation> getRepairs(int selectedScooterID) {
        return DbAccess.getRepairs(selectedScooterID);
    }

    public static boolean isReloader(int id) {
        return DbAccess.isReloader(id);
    }

    public static ArrayList<Object> getDataFromCurrentReload(int userID) {
        return DbAccess.getDataFromCurrentReload(userID);
    }

    public static ArrayList<User> getUsers() {
        return DbAccess.getUsers();
    }

    public static ArrayList<Scooter> getScooterWithAtLeastTenPlaints(){
        return DbAccess.getScooterWithAtLeastTenPlaints();
    }


    public static ArrayList<Scooter> getScooterWithAtLeast1150Trips(){
        return DbAccess.getScootersWithAtLeast1150Trips();
    }


    public static ArrayList<Scooter> getScooterWithUnresolvedComplaints(){
        return DbAccess.getScootersWithUnresolvedComplaints();
    }

    public static ArrayList<Scooter> getClosestScootersFromPoint(Double latitude, Double longitude, int numScooters) throws SQLException {
        return DbAccess.getClosestScootersFromPoint(latitude, longitude, numScooters);
    }

    public static Reloader getReloader(int id) {
        return DbAccess.getReloader(id);
    }

    public static void deleteUser(int selectedUserID) {
        DbAccess.deleteUser(selectedUserID);
    }

    public static void upgradeUser(int id, String firstname, String surname, String phone,
                                   String adNum, String adCity, String adCP, String adStreet) {
        DbAccess.upgradeUser(id, firstname, surname, phone, adNum, adCity, adCP, adStreet);
    }

    public static Scooter getClosestScooter(Double latitude, Double longitude) throws SQLException {
        return DbAccess.getClosestScooterFromPoint(latitude, longitude);
    }

    public static ArrayList<Scooter> orderByNumReparations() {
        return DbAccess.orderScootersByNumReparations();
    }

    public static ArrayList<User> usersOrderedByTrips() {
        return DbAccess.usersOrderedByTrips();
    }

    public static ArrayList<User> getUserWithAtLeastTenTripsAndData(){
        return DbAccess.getUserWithAtLeastTenTripsAndData();
    }

    public static ArrayList<Scooter> orderByTotalDist() throws SQLException {
        return DbAccess.orderScootersByLongestDistance();
    }


}
