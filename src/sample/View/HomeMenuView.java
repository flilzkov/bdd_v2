package sample.View;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import sample.Controller.ViewController;


public class HomeMenuView {

    Scene scene;


    public HomeMenuView(int width, int height){

        GridPane menu = new GridPane();
        menu.setVgap(80);
        menu.setHgap(80);
        Insets insets = new Insets(100, 20,20,50);
        menu.setPadding(insets);
        Button userButton = new Button("USER");
        Button technicianButton = new Button("TECHNICIAN");
        userButton.setOnAction(event -> {
            ViewController.goToUserMenu();
        });
        technicianButton.setOnAction(event -> {
            ViewController.goToTechMenu();
        });
        menu.add(userButton, 0, 0, 1, 1);
        menu.add(technicianButton, 1, 0, 1, 1);

        scene = new Scene(menu, width, height);
    }

    public Scene getScene(){
        return scene;
    }


}
