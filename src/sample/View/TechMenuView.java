package sample.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import sample.Controller.DbController;
import sample.Controller.ViewController;

public class TechMenuView {

    Scene scene;
    TextField techIdText;
    TextField passwordText;

    public TechMenuView(int width, int height){


        GridPane loginMenu = new GridPane();

        Button returnButton = new Button("back to Home Menu");
        returnButton.setOnAction(event ->  ViewController.goToHomeMenu());


        techIdText = new TextField();
        techIdText.setPromptText("tech id");
        passwordText = new PasswordField();
        passwordText.setPromptText("password");


        Button loginButton = new Button("login");
        loginButton.setOnAction(event -> loginTech());

        loginMenu.add(techIdText, 0,0,1,1);
        loginMenu.add(passwordText, 0,1,1,1);
        loginMenu.add(loginButton, 1,0,1,1);
        loginMenu.add(returnButton, 2,1,1,1);

        scene = new Scene(loginMenu, width, height);


    }

    private void loginTech() {
        // check from validity
        String regex = "\\d+";
        if (techIdText.getText().matches(regex) && !passwordText.getText().equals("")) {

            // check credentials validity
            if (DbController.checkIdMatchPasswordMechanic(techIdText.getText(), passwordText.getText())){
                ViewController.goToTechDecisionPage(techIdText.getText());
            }
            else {
                PopUpMessageView.showError("Invalid credentials :\n id or password incorrect");
            }
        } else {
            PopUpMessageView.showError("invalid form :\nuser id must be a number\npassword cannot be empty");
        }
    }

    public Scene getScene() {
        return scene;
    }



}
