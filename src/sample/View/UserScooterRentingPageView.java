package sample.View;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import sample.Controller.DbController;
import sample.Controller.ViewController;
import sample.Model.DbAccess;
import sample.Model.Scooter;
import sample.Model.Trip;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserScooterRentingPageView {

    boolean initFind = true;
    Scene scene;
    GridPane userPage;
    Label scooterDataLabel;
    Label tripDataLabel;
    ListView<String> scooterListView;
    ListView<String> tripListView;
    ObservableList<String> scootersID;
    ObservableList<String> tripsID;
    Button addPlaintButton;
    Button rentScooterButton;
    Button reloadScooterButon;
    int selectedScooterID;
    boolean isReloader;
    Button scootersButton;
    Button activityPageButton;
    Button tripButton;
    Text reloaderText;
    Button findClosestScooterButton;
    TextField latitudeText;
    TextField longitudeText;
    private ObservableList<String> filters;
    private ComboBox filterComboBox;
    private ArrayList<Scooter> scooters;
    private Double latitude, longitude;

    public UserScooterRentingPageView(int userID, boolean _isReloader, int width, int height){

        reloaderText = new Text();
        activityPageButton = new Button("my activity");
        activityPageButton.setOnAction(e ->  ViewController.goToUserActivityPage(userID));
        userPage = new GridPane();
        isReloader = _isReloader;
        if (isReloader){ reloaderText.setText("you can reload scooters");}
        userPage.setHgap(20);

        rentScooterButton = new Button("rent scooter");
        rentScooterButton.setVisible(false);
        rentScooterButton.setOnAction(event -> {
            if (DbController.getDataFromCurrentRent(userID) != null){
                PopUpMessageView.showError("you are already renting a scooter");
            } else {
                DbController.rentScooter(selectedScooterID, userID);
                PopUpMessageView.showMessage("thank you for trusting our service");
                scootersButton.fire();
            }
        });

        reloadScooterButon = new Button("reload scooter");
        reloadScooterButon.setVisible(false);
        reloadScooterButon.setOnAction(event -> {

            if (DbController.getDataFromCurrentReload(userID) != null){
                PopUpMessageView.showError("you aleady have one scooter in reload");
                return;
            } else if (DbController.takeScooterForReload(selectedScooterID, userID)){
                PopUpMessageView.showMessage("thank you for your participation");
                scootersButton.fire();
            } else {
                PopUpMessageView.showError("this scooter is already full");
            }
        });


        Label userIdLabel = new Label("user : "+ userID);

        Button returnButton = new Button("back to User Menu");
        returnButton.setOnAction(event ->  ViewController.goToUserMenu() );

        scootersButton = new Button("available scooters");
        scootersButton.setOnAction(event -> {
            scooters = null;
            try {
                scooters = DbController.getAvailableScooters();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            showAvailableScooters();
        });

        tripButton = new Button("trip history");
        tripButton.setOnAction(event -> {
            ArrayList<Trip> tripData = null;
            try {
                tripData = DbAccess.getTripHistoryUser(userID);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            showTripHistory(tripData);
        });

        addPlaintButton = new Button("add plaint");
        addPlaintButton.setVisible(false);
        addPlaintButton.setOnAction(event -> {
            if (!DbController.addPlaint(selectedScooterID, userID)){
                PopUpMessageView.showError("You have already submitted a\ncomplaint for this scooter.\nWe are working on it");
            } else {
                scootersButton.fire();
            }
        });

        findClosestScooterButton = new Button("find closest");
        findClosestScooterButton.setOnAction(event -> submitPos());
        findClosestScooterButton.setVisible(false);

        userPage.add(returnButton, 3,10,1,1);
        userPage.addRow(1, new Text(""));
        userPage.add(userIdLabel, 0,0,1,1);
        userPage.add(scootersButton, 0,3,1,1);
        userPage.add(tripButton, 3,3,1,1);


        scooterDataLabel = new Label();
        tripDataLabel = new Label();
        scooterListView = new ListView<>();
        tripListView = new ListView<>();
        scooterListView.setVisible(false);
        tripListView.setVisible(false);
        userPage.add(reloaderText, 1, 0);
        userPage.add(scooterDataLabel,0,5);
        userPage.add(scooterListView, 0, 4);
        userPage.add(rentScooterButton,0,6);
        userPage.add(addPlaintButton, 0,7);
        userPage.add(reloadScooterButon,0,8);
        userPage.add(findClosestScooterButton, 1,2);
        userPage.add(tripListView, 3, 4);
        userPage.add(tripDataLabel,3, 5);
        userPage.add(activityPageButton, 3, 0);

        scene = new Scene(userPage, width, height);

    }

    private void showTripHistory(ArrayList<Trip> trips){

        ArrayList<String> tripsIDls = new ArrayList<>();
        for (Trip trip : trips){
            tripsIDls.add(String.valueOf(trip.getStarttime()));
        }
        tripsID = FXCollections.observableArrayList(tripsIDls);

        tripListView.setItems(tripsID);


        tripListView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                tripDataLabel.setText(
                        "start date : " + trips.get(newValue.intValue()).getStarttime() + "\n"
                                + "end date :" + trips.get(newValue.intValue()).getEndtime() + "\n"
                                + "from : (" + trips.get(newValue.intValue()).getSourceX() + ", "
                                + trips.get(newValue.intValue()).getSourceY() + ")\n"
                                + "to : (" + trips.get(newValue.intValue()).getDestinationX() + ", "
                                + trips.get(newValue.intValue()).getDestinationY() + ")\n"
                                + "on scooter :" + trips.get(newValue.intValue()).getScooter()
                );

            }
        });
        tripListView.setVisible(true);

    }

    private void makeFiltersComboBox() {
        ArrayList<String> filtersLs = new ArrayList<>();
        filtersLs.add("no filter");
        filtersLs.add("10 closest");
        filtersLs.add("100 closest");
        filters = FXCollections.observableArrayList(filtersLs);

        filterComboBox = new ComboBox(filters);
        filterComboBox.setPromptText("filter by ...");

        filterComboBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                if (newValue.intValue() == 0) {
                    try {
                        scooters = DbController.getScooters();
                        showAvailableScooters();
                        System.out.println("no filter");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else if (newValue.intValue() == 1) {
                    try {
                        scooters = DbController.getClosestScootersFromPoint(latitude, longitude,10);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    showAvailableScooters();
                    System.out.println("10 closest");

                } else if (newValue.intValue() == 2) {
                    try {
                        scooters = DbController.getClosestScootersFromPoint(latitude, longitude,100);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    showAvailableScooters();
                    System.out.println("100 closest");
                }
            }
        });
        filterComboBox.setVisible(false);
        userPage.add(filterComboBox, 1,3);
    }

    private void openFindClosestBox() {
        latitudeText = new TextField();
        longitudeText = new TextField();
        latitudeText.setPromptText("latitude (50.76-50.91)");
        longitudeText.setPromptText("longitude (4.24-4.48)");
        latitudeText.setVisible(false);
        longitudeText.setVisible(false);
        HBox formPosition = new HBox(latitudeText, longitudeText);

        userPage.add(formPosition,0,2);
        initFind = false;
    }

    private void submitPos() {
        final String regexPos = "\\d+.\\d+";
        Scooter scooter = new Scooter();
        if (latitudeText.getText().matches(regexPos) && longitudeText.getText().matches(regexPos)) {
            try {
                latitude = Double.valueOf(latitudeText.getText());
                longitude = Double.valueOf(longitudeText.getText());
                scooter = DbController.getClosestScooter(latitude, longitude);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            PopUpMessageView.showMessage("Closest scooter has id "+scooter.getId()+
                    "\nIt is at longitude "+scooter.getPosX()+
                    "\nand latitude "+scooter.getPosY());
            makeFiltersComboBox();
            filterComboBox.setVisible(true);
        } else {
            PopUpMessageView.showError("longitude and latitude\nmust be float");
        }
    }

    private void showAvailableScooters(){

        ArrayList<String> scootersIDls = new ArrayList<>();
        for (Scooter scooter : scooters) {
            String id = String.valueOf(scooter.getId());
            if(scooter.getDist()!=-1.0) id+=(" ("+(double)Math.round(scooter.getDist()*100)/100+" km)");
            scootersIDls.add(id);
        }
        scootersID = FXCollections.observableArrayList(scootersIDls);


        scooterListView.setItems(new SortedList<>(scootersID));

        scooterListView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                scooterDataLabel.setText(
                        "id : " + scooters.get(newValue.intValue()).getId() + "\n"
                                + "date :" + scooters.get(newValue.intValue()).getDate() + "\n"
                                + "model :" + scooters.get(newValue.intValue()).getModel() + "\n"
                                + "plaint :" + String.valueOf(DbController.hasUnresolvedComplaint(scooters.get(newValue.intValue()).getId())) + "\n"
                                + "load :" + scooters.get(newValue.intValue()).getLoad() + "\n"
                                + "lattitude :" + scooters.get(newValue.intValue()).getPosY() + "\n"
                                + "longitude :" + scooters.get(newValue.intValue()).getPosX() + "\n"

                );
                addPlaintButton.setVisible(true);
                rentScooterButton.setVisible(true);
                findClosestScooterButton.setVisible(true);
                if(initFind) openFindClosestBox();
                latitudeText.setVisible(true);
                longitudeText.setVisible(true);
                findClosestScooterButton.setVisible(true);
                if (isReloader) {reloadScooterButon.setVisible(true); }
                selectedScooterID = scooters.get(newValue.intValue()).getId();
            }
        });
        findClosestScooterButton.setVisible(true);
        if(initFind) openFindClosestBox();
        latitudeText.setVisible(true);
        longitudeText.setVisible(true);
        scooterListView.setVisible(true);


    }

    public Scene getScene(){
        return scene;
    }

}
