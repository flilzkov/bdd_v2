package sample.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.Controller.DbController;
import sample.Controller.ViewController;

public class UserMenuView {

    Scene scene;
    TextField idText;
    TextField passwordText;


    public UserMenuView(int width, int height){


        GridPane loginMenu = new GridPane();
        loginMenu.setHgap(10);

        Button returnButton = new Button("back to Home Menu");
        returnButton.setOnAction(event ->  ViewController.goToHomeMenu() );

        idText = new TextField();
        idText.setPromptText("user ID");
        passwordText = new PasswordField();
        passwordText.setPromptText("password");

        Text newUserText = new Text("\n\nnew user ? Register !");
        Text existingUserText = new Text("existing user ? Login !");

        Button loginButton = new Button("login");
        loginButton.setOnAction(event -> loginUser());

        Button registerButton = new Button("register");
        registerButton.setOnAction(event -> ViewController.goToUserRegister());

        loginMenu.add(existingUserText, 0,0);
        loginMenu.add(newUserText,0,3);
        loginMenu.add(idText, 0,1);
        loginMenu.add(registerButton, 0,4);
        loginMenu.add(passwordText,0,2);
        loginMenu.add(loginButton, 1,2);
        loginMenu.add(returnButton, 2,6);



        scene = new Scene(loginMenu, width, height);


    }

    private void loginUser() {
        // check from validity
        String regex = "\\d{1,7}";


        if (idText.getText().matches(regex) && !passwordText.getText().equals("")) {

            int userId = Integer.valueOf(idText.getText());
            // check credentials validity
            if (DbController.checkIdMatchPassword(userId, passwordText.getText())){

                ViewController.goToUserActivityPage(userId);

            }
            else {
                PopUpMessageView.showError("Invalid credentials :\n id or password incorrect");
            }
        } else {
            PopUpMessageView.showError("invalid form :\nuser id must be a number\npassword cannot be empty");
        }
    }

    public Scene getScene() {
        return scene;
    }





}
