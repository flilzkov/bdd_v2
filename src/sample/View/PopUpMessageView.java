package sample.View;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PopUpMessageView {



    public static void showMessage(String message) {
        showPopUpWindow("message", message, Color.BLACK);
    }

    public static void showError(String message){
        showPopUpWindow("error", message, Color.RED);
    }

    public static void showPopUpWindow(String title, String message, Color color){
        Stage errWindow = new Stage();
        errWindow.setTitle(title);
        errWindow.initModality(Modality.APPLICATION_MODAL);
        GridPane pane = new GridPane();
        Text errText = new Text(message);
        errText.setFill(color);
        pane.add(errText, 0, 0);
        pane.setAlignment(Pos.CENTER);
        errWindow.setScene(new Scene(pane, 200, 100));
        errWindow.showAndWait();

    }

    public static boolean confirmChoice(String message){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        return alert.getResult() == ButtonType.YES;
    }



}
