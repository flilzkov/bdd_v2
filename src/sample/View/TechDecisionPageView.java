package sample.View;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import sample.Controller.ViewController;

public class TechDecisionPageView {


    Scene scene;


    public TechDecisionPageView(String id, int width, int height){

        GridPane menu = new GridPane();
        menu.setVgap(80);
        menu.setHgap(80);
        Insets insets = new Insets(100, 20,20,50);
        menu.setPadding(insets);
        Button returnButton = new Button("back to Tech Menu");
        Button userButton = new Button("USER");
        Button scooterButton = new Button("SCOOTER");
        returnButton.setOnAction(event -> {
            ViewController.goToTechMenu();
        });
        userButton.setOnAction(event -> {
            ViewController.goToTechUserPage(id);
        });
        scooterButton.setOnAction(event -> {
            ViewController.goToTechScooterPage(id);
        });
        menu.add(userButton, 0, 0, 1, 1);
        menu.add(scooterButton, 1, 0, 1, 1);
        menu.add(returnButton, 1,1);

        scene = new Scene(menu, width, height);
    }

    public Scene getScene(){
        return scene;
    }



}
