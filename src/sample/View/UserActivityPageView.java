package sample.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import sample.Controller.DbController;
import sample.Controller.ViewController;
import sample.Model.DbAccess;
import sample.Model.Scooter;

import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class UserActivityPageView {

    Scene scene;

    VBox vBox;
    int userID;

    Button returnScooterButton;
    Button refreshPriceButton;
    Button returnButton;
    HBox activity1HBox;
    HBox activity2HBox;
    Date actualTime;
    String strDate;

    Button rentScooterButton;


    public UserActivityPageView(int userID, ArrayList<Object> tripActivity, ArrayList<Object> reloadActivity, int width, int height){

        this.userID = userID;
        vBox = new VBox();
        vBox.setSpacing(20);

        Text helloText = new Text("  Hello user : " + userID);

        rentScooterButton = new Button("Rent scooter");
        rentScooterButton.setOnAction(e -> ViewController.goToUserScooterRentingPage(userID));
        returnButton = new Button("Back to user Menu");
        returnButton.setOnAction(event -> ViewController.goToUserMenu());

        ArrayList<Pane> selection = new ArrayList<>();

        if (tripActivity != null){
            selection.add(getTripHBox((int) tripActivity.get(1), (String) tripActivity.get(2), (float) tripActivity.get(3)));
        }
        if (reloadActivity != null) {
            selection.add(getReloadHBox((int) reloadActivity.get(1), (int) reloadActivity.get(2), (String) reloadActivity.get(3)));
        } if (tripActivity == null && reloadActivity == null){
            selection.add(getNoActivityHBox());
        }

        vBox.getChildren().add(helloText);
        vBox.getChildren().addAll(selection);
        vBox.getChildren().addAll(rentScooterButton, returnButton);
        scene = new Scene(vBox, width, height);

    }


    public HBox getNoActivityHBox(){
        HBox noActivityHBox = new HBox(new Text("  You have no activity right now."));
        return noActivityHBox;
    }



    private HBox getReloadHBox(int reloadScooterID, int initialLoad, String reloadStartTime){

        Label reloadLabel = new Label("  You are reloading scooter : " + String.valueOf(reloadScooterID) + "\n"
                + "  Since : " + String.valueOf(reloadStartTime) + "\n"
                + "  Initial load was : " + String.valueOf(initialLoad));

        returnScooterButton = new Button("Return scooter");
        returnScooterButton.setOnAction(event -> {
            DbController.returnScooterFromReload(reloadScooterID, userID);
            PopUpMessageView.showMessage("Thank you for your service !\nMoney will soon be transfered\n" +
                    "to your bank account");
            ViewController.goToUserActivityPage(userID);
        });

        HBox reloadHBox = new HBox(reloadLabel, returnScooterButton);
        reloadHBox.setSpacing(100);

        return reloadHBox;

    }

    private HBox getTripHBox(int tripScooterID, String tripStartTime, float tripPrice){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        actualTime = new Date(new java.util.Date().getTime());
        strDate = format.format(actualTime);

        Label tripLabel = new Label("  You are on a trip on scooter : " + String.valueOf(tripScooterID) + "\n"
                + "  Since : " + String.valueOf(tripStartTime) + "\n"
                + "  Current price is : " + String.valueOf(tripPrice) + "€\n"
                + "  Calculated for current time :" + strDate + "\n");

        returnScooterButton = new Button("Return scooter");
        returnScooterButton.setOnAction(event -> {
            DbController.returnScooter(tripScooterID, userID);
            PopUpMessageView.showMessage("Thank you, we hope you had\na good ride !\nSee you soon");
            ViewController.goToUserActivityPage(userID);
        });


        refreshPriceButton = new Button("Refresh price");
        refreshPriceButton.setOnAction(event -> {
            java.util.Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            String strDate = dateFormat.format(date);
            actualTime = new Date(new java.util.Date().getTime());
            float newPrice = DbAccess.calculatePrice(tripStartTime,strDate);
            tripLabel.setText("  You are on a trip on scooter : " + String.valueOf(tripScooterID) + "\n"
                    + "  Since : " + String.valueOf(tripStartTime) + "\n"
                    + "  Current price is : " + String.valueOf(newPrice) + "€\n"
                    + "  Calculated for current time :" + strDate);
        });
        HBox tripHBox = new HBox(tripLabel, refreshPriceButton, returnScooterButton);
        return tripHBox;

    }




    public Scene getScene() {
        return scene;
    }
}
