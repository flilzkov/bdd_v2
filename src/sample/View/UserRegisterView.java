package sample.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import sample.Controller.DbController;
import sample.Controller.ViewController;



public class UserRegisterView {

    Scene scene;
    CheckBox reloaderCheckBox;

    TextField nameText = new TextField();
    TextField surnameText = new TextField();
    TextField adNumText = new TextField();
    TextField adStreetText = new TextField();
    TextField adCityText = new TextField();
    TextField adPostCodeText = new TextField();
    TextField phoneText = new TextField();


    public UserRegisterView(int width, int height){

        GridPane registerMenu = new GridPane();

        reloaderCheckBox = new CheckBox();


        nameText.setPromptText("name");
        nameText.setVisible(false);
        surnameText.setPromptText("surname");
        surnameText.setVisible(false);
        adNumText.setPromptText("number");
        adNumText.setVisible(false);
        adStreetText.setPromptText("street");
        adStreetText.setVisible(false);
        adCityText.setPromptText("city");
        adCityText.setVisible(false);
        adPostCodeText.setPromptText("post code");
        adPostCodeText.setVisible(false);
        phoneText.setPromptText("phone");
        phoneText.setVisible(false);

        Button returnButton = new Button("back to User Menu");
        returnButton.setOnAction(event ->  ViewController.goToUserMenu() );

        TextField passwordText = new TextField();
        passwordText.setPromptText("password");
        TextField bankAccountText = new TextField();
        bankAccountText.setPromptText("bank account");

        Button registerButton = new Button("register");
        registerButton.setOnAction(event -> {
            final String regexBank = "\\d{16}";
            final String regexPhone = "\\d{10,14}";
            final String regexPostCode = "\\d{4}";
            final String regexNum = "\\d+";
            if (reloaderCheckBox.isSelected()) {
                if (bankAccountText.getText().matches(regexBank) && !passwordText.getText().equals("") &&
                        !nameText.getText().equals("") && !surnameText.getText().equals("") &&
                        !adNumText.getText().equals("") && !adCityText.getText().equals("") &&
                        !adPostCodeText.getText().equals("") && !adStreetText.getText().equals("") &&
                        !phoneText.getText().equals("") && phoneText.getText().matches(regexPhone) &&
                        adPostCodeText.getText().matches(regexPostCode) && adNumText.getText().matches(regexNum))
                {
                    int id = DbController.registerReloader(nameText.getText(), surnameText.getText(), passwordText.getText(),
                            phoneText.getText(), adNumText.getText(), adCityText.getText(), adPostCodeText.getText(),
                            adStreetText.getText(), bankAccountText.getText());
                    ViewController.goToUserScooterRentingPage(id);
                } else {
                    PopUpMessageView.showError("incorrect form :\nplease fill all fields\nbank account must contain 16 digits\n" +
                                                "phone must contain 10-14 digits\npost code must contain 4 digits");
                }
            }
            else {
                if (bankAccountText.getText().matches(regexBank) && !passwordText.getText().equals("")) {
                    int id = DbController.registerAnonymeUser(passwordText.getText(), bankAccountText.getText());
                    ViewController.goToUserScooterRentingPage(id);
                } else {
                    PopUpMessageView.showError("incorrect form :\npassword cannot be null\nbank account must contain 16 digits");
                }
            }
        });

        reloaderCheckBox.setText("Register as Reloader");
        reloaderCheckBox.setOnAction(event -> {
            boolean visible = reloaderCheckBox.isSelected();
            nameText.setVisible(visible);
            surnameText.setVisible(visible);
            adNumText.setVisible(visible);
            adStreetText.setVisible(visible);
            adCityText.setVisible(visible);
            adPostCodeText.setVisible(visible);
            phoneText.setVisible(visible);
        });

        registerMenu.add(passwordText, 0,0);
        registerMenu.add(bankAccountText, 0,1);
        registerMenu.add(registerButton, 1,0);
        registerMenu.add(reloaderCheckBox, 1, 1);
        registerMenu.add(returnButton, 1,2);
        registerMenu.add(nameText, 0, 2);
        registerMenu.add(surnameText, 0, 3);
        registerMenu.add(adStreetText, 0, 4);
        registerMenu.add(adNumText, 0, 5);
        registerMenu.add(adPostCodeText, 0, 6);
        registerMenu.add(adCityText, 0, 7);
        registerMenu.add(phoneText,0,8);


        scene = new Scene(registerMenu, width, height);

    }
    public Scene getScene() {
        return scene;
    }


}
