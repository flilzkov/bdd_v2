package sample.View;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.Controller.DbController;
import sample.Controller.ViewController;
import sample.Model.Reparation;
import sample.Model.Scooter;

import java.sql.SQLException;
import java.util.ArrayList;

public class TechScooterPageView {

    int selectedScooterID;
    String selectedRepairTime, tech_id;

    GridPane techPage;
    Scene scene;

    boolean initScooters = true, initReparations = true;

    Label scooterDataLabel, repairDataLabel = new Label();
    Button changeStatusButton, deleteScooterButton, resolvePlaintButton, showScootersButton,
            showRepairsButton, treatPlaintButton;
    TextArea noteText;
    ComboBox filterComboBox;
    ArrayList<String> scootersIDls, repairsTimels;
    ArrayList<Scooter> scooters;
    ArrayList<Reparation> repairs;
    ObservableList<String> scootersID, repairsTimes, filters;
    ListView<String> listViewScooters = new ListView<>(), listViewReparations = new ListView<>();

    public TechScooterPageView(String tech_id, int width, int height){

        this.tech_id = tech_id;

        techPage = new GridPane();
        scene = new Scene(techPage, width, height);

        Text addScooterText = new Text("Add a new scooter?");

        TextField modelText = new TextField();
        modelText.setPromptText("model");
        TextField idText = new TextField();
        idText.setPromptText("scooter id");

        Button returnButton = new Button("back to Decision Page");
        returnButton.setOnAction(event ->  ViewController.goToTechDecisionPage(tech_id) );

        Button addScooterButton = new Button("add scooter");
        addScooterButton.setOnAction(event -> {
            addScooter(idText, modelText);
            if ( listViewScooters.isVisible() )
                updateScootersListView();
        });

        showScootersButton = new Button("show existing scooters");
        showScootersButton.setOnAction(event -> {
            if (initScooters || !listViewScooters.isVisible()) {
                showScooters();
            } else {
                setReparationsVisible(false);
                setScootersListVisible(false);
                setComplaintTreatmentVisible(false);

            }
        });

        techPage.add(addScooterText, 0, 0);
        techPage.add(returnButton, 2,0,1,1);
        techPage.add(idText, 0, 1, 1,1);
        techPage.add(modelText, 0, 2, 1,1);
        techPage.add(addScooterButton, 1, 2, 1,1);
        techPage.add(showScootersButton, 0, 3, 1, 1);
    }

    private void addScooter(TextField idText, TextField modelText) {
        // check from validity
        String regex = "\\d+";
        if (idText.getText().matches(regex) && !modelText.getText().equals("")) {
            int id = Integer.valueOf(idText.getText());
            // check credentials validity
            try {
                if (!DbController.checkScooterExists(id)){
                    DbController.addScooter(id, modelText.getText());
                    PopUpMessageView.showMessage("The scooter has been added");
                    scooters = DbController.getScooters();
                }
                else {
                    PopUpMessageView.showError("Scooter ID already used");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            PopUpMessageView.showError("invalid form :\nscooter id must be a number\nmodel cannot be empty");
        }
    }

    private void showScooters(){


        if (initScooters) {
            scooterDataLabel = new Label();
            makeScootersListView();
            // makeSortingComboBox();
            makeFiltersComboBox();
            makeChangeStatusButton();
            makeShowReparationsButton();
            makeTreatPlaintButton();
            makeDeleteButton();

            initScooters = false;
            try {
                scooters = DbController.getScooters();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        updateScootersListView();
        setScootersListVisible(true);
    }

    private void showReparations(){

        updateReparations();

        if (initReparations) {
            makeReparationsListView();

            techPage.add(repairDataLabel, 2,4);

            initReparations = false;
        }

        setReparationsVisible(true);
    }

    private void updateScootersListView() {

        scootersIDls = new ArrayList<>();

        for (Scooter scooter : scooters) {
            String id = String.valueOf(scooter.getId());
            if (scooter.getNumReparations() != -1) id += (" ("+scooter.getNumReparations()+")");
            else if (scooter.getTrips() != -1) id += (" ("+scooter.getTrips()+")");
            else if (scooter.getDist() != -1.0) id += (" ("+(double)Math.round(scooter.getDist()*100)/100+" km)");
            scootersIDls.add(id);
        }
        scootersID = FXCollections.observableArrayList(scootersIDls);

        listViewScooters.setItems(scootersID);
    }

    private void updateReparations() {

        repairs = null;
        repairsTimels = new ArrayList<>();

        repairs = DbController.getRepairs(selectedScooterID);

        for (Reparation repair : repairs) {
            repairsTimels.add(String.valueOf(repair.getRepairTime()));
        }
        repairsTimes = FXCollections.observableArrayList(repairsTimels);

        listViewReparations.setItems(repairsTimes);
    }

    private void makeTreatPlaintButton() {
        treatPlaintButton = new Button("treat plaint");
        treatPlaintButton.setOnAction(event -> {
            setReparationsVisible(false);
            setComplaintTreatmentVisible(true);
        });
        treatPlaintButton.setVisible(false);

        noteText = new TextArea();
        noteText.setPromptText("Note on complaint");
        noteText.setMaxSize(200,200);

        resolvePlaintButton = new Button("resolve");
        resolvePlaintButton.setOnAction(event -> {
            DbController.treatPlaint(selectedScooterID, tech_id, noteText.getText());
            PopUpMessageView.showMessage("The complaint has been resolved");
            updateScootersListView();
        });

        setComplaintTreatmentVisible(false);

        techPage.add(treatPlaintButton,1, 7);
        techPage.add(noteText, 1,3,1,2);
        techPage.add(resolvePlaintButton, 2,4,1,1);
    }

    private void makeShowReparationsButton() {
        showRepairsButton = new Button("show reparations");
        showRepairsButton.setOnAction(event -> {
            setComplaintTreatmentVisible(false);
            if ( !listViewReparations.isVisible() ){
                showReparations();
            } else {
                setReparationsVisible(false);
            }
        });
        showRepairsButton.setVisible(false);

        techPage.add(showRepairsButton, 0,7);
    }

    private void makeFiltersComboBox() {
        ArrayList<String> filtersLs = new ArrayList<>();
        filtersLs.add("no filter");
        filtersLs.add("10+ plaints");
        filtersLs.add("1200+ trips");
        filtersLs.add("unresolved complaints");
        filtersLs.add("sort by # reparations");
        filtersLs.add("sort by cumulated distance");
        filters = FXCollections.observableArrayList(filtersLs);

        filterComboBox = new ComboBox(filters);
        filterComboBox.setPromptText("filter by ...");

        filterComboBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                if ( newValue.intValue() == 0){
                    try {
                        scooters = DbController.getScooters();
                        updateScootersListView();
                        System.out.println("no filter");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }
                else if ( newValue.intValue() == 1 ){
                    scooters = DbController.getScooterWithAtLeastTenPlaints();
                    updateScootersListView();
                    System.out.println("10+ plaints");

                }
                else if ( newValue.intValue() == 2 ){
                    scooters = DbController.getScooterWithAtLeast1150Trips();
                    updateScootersListView();
                    System.out.println("1200+ trips");
                }
                else if ( newValue.intValue() == 3){
                    scooters = DbController.getScooterWithUnresolvedComplaints();
                    updateScootersListView();
                    System.out.println("unresolved complaints");
                }
                else if ( newValue.intValue() == 4){
                    scooters = DbController.orderByNumReparations();
                    updateScootersListView();
                    System.out.println("Sort by #reparations");
                }
                else if ( newValue.intValue() == 5){
                    try {
                        scooters = DbController.orderByTotalDist();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    updateScootersListView();
                }
            }
        });

        techPage.add(filterComboBox, 1,3);
    }

    /*private void makeSortingComboBox() {
        ArrayList<String> sortingsLs = new ArrayList<String>();
        sortingsLs.add("sort by # reparations");
        sortingsLs.add("sort by B");
        sortings = FXCollections.observableArrayList(sortingsLs);

        sortingComboBox = new ComboBox(sortings);
        sortingComboBox.setPromptText("sort by ...");

        sortingComboBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                if ( newValue.intValue() == 0 ) {
                    scooters = DbController.orderByNumReparations();
                    updateScootersListView();
                    System.out.println("Sort by #reparations");
                }
                else if ( newValue.intValue() == 1 )
                    System.out.println("Sort by B");

            }
        });

        techPage.add(sortingComboBox, 2,3);
    }*/

    private void makeReparationsListView() {
        listViewReparations.setPrefSize(150, 230); // shows 10 items

        listViewReparations.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                selectNewReparations(newValue);
            }
        });

        techPage.add(listViewReparations, 1,4);
    }

    private void makeDeleteButton() {
        deleteScooterButton = new Button("delete scooter");
        deleteScooterButton.setOnAction(event -> {

            if (PopUpMessageView.confirmChoice("Are you sure ? Deleting a scooter will\n" +
                    "result in the loss of any data associated\n" +
                    "such as trips, complaints, reloads and repairs")){

                try {
                    DbController.deleteScooter(selectedScooterID);
                    scooters = DbController.getScooters();
                    updateScootersListView();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                updateScootersListView();
            }
        });
        deleteScooterButton.setVisible(false);

        techPage.add(deleteScooterButton, 0, 6);
    }

    private void makeChangeStatusButton() {
        changeStatusButton = new Button("change status");
        changeStatusButton.setOnAction(event -> {
            DbController.changeScooterStatus(selectedScooterID);
            updateScootersListView();
        });
        changeStatusButton.setVisible(false);

        techPage.add(changeStatusButton, 1, 6);
    }

    private void makeScootersListView() {
        listViewScooters.setPrefSize(150, 230); // shows 10 items

        listViewScooters.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                selectNewScooter(newValue);
            }
        });

        techPage.add(listViewScooters, 0, 4);
        techPage.add(scooterDataLabel,0,5);
    }

    private void makeScooterDataLabel(Number newValue) {
        scooterDataLabel.setText(
                "id : " + scooters.get(newValue.intValue()).getId() + "\n"
                        + "date : " + scooters.get(newValue.intValue()).getDate() + "\n"
                        + "model : " + scooters.get(newValue.intValue()).getModel() + "\n"
                        + "plaint :" + String.valueOf(DbController.hasUnresolvedComplaint(scooters.get(newValue.intValue()).getId())) + "\n"
                        + "discharged : " + String.valueOf(DbController.isUnload(scooters.get(newValue.intValue()).getId())) + "\n"
                        + "availability : " + String.valueOf(DbController.getScooterStatus(scooters.get(newValue.intValue()).getId()))
        );
    }

    private void selectNewScooter(Number newValue) {
        setComplaintTreatmentVisible(false);
        setReparationsVisible(false);
        makeScooterDataLabel(newValue);
        selectedScooterID = scooters.get(newValue.intValue()).getId();
        if ( DbController.hasUnresolvedComplaint(scooters.get(newValue.intValue()).getId()) ){
            treatPlaintButton.setVisible(true);
        }
        else treatPlaintButton.setVisible(false);

        changeStatusButton.setVisible(true);
        deleteScooterButton.setVisible(true);
        showRepairsButton.setVisible(true);
        scooterDataLabel.setVisible(true);
    }

    private void selectNewReparations(Number newValue) {
        String text =  "repair date :\n" + repairs.get(newValue.intValue()).getRepairTime() + "\n"
                + "complaint date :\n" + repairs.get(newValue.intValue()).getComplainTime() + "\n"
                + "mechanic :\n" + repairs.get(newValue.intValue()).getMechanic() + "\n"
                + "user who filed :\n" + repairs.get(newValue.intValue()).getUser() + "\n";

        if ( !repairs.get(newValue.intValue()).getNote().equals("") ) text += "note :\n"+ repairs.get(newValue.intValue()).getNote();
        repairDataLabel.setText(text);
        selectedRepairTime = repairs.get(newValue.intValue()).getRepairTime();
    }

    private void setScootersListVisible(boolean b) {
        listViewScooters.setVisible(b);
        // sortingComboBox.setVisible(b);
        filterComboBox.setVisible(b);
        hideScooterControl();

        if (b) showScootersButton.setText("hide existing scooters");
        else showScootersButton.setText("show existing scooters");
    }

    private void hideScooterControl() {
        changeStatusButton.setVisible(false);
        deleteScooterButton.setVisible(false);
        showRepairsButton.setVisible(false);
        scooterDataLabel.setVisible(false);
    }

    private void setReparationsVisible(boolean b) {
        listViewReparations.setVisible(b);
        repairDataLabel.setVisible(b);
        if (b) showRepairsButton.setText("hide reparations");
        else showRepairsButton.setText("show reparations");
    }

    private void setComplaintTreatmentVisible(boolean b) {
        noteText.setVisible(b);
        resolvePlaintButton.setVisible(b);
    }



    public Scene getScene() {
        return scene;
    }
}
