package sample.View;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import sample.Controller.DbController;
import sample.Controller.ViewController;
import sample.Model.Reloader;
import sample.Model.Scooter;
import sample.Model.User;

import java.util.ArrayList;


public class TechUserPageView {

    private Scene scene;
    private GridPane techPage;

    private String tech_id;
    private boolean initUsers = true, initForm = true;
    private int selectedUserID;

    private Button showUsersButton, showUserDataButton, upgradeUserButton, deleteUserButton, submitButton;
    private TextField bankAccountText, nameText, surnameText, adNumText, adStreetText,
            adCityText, adPostCodeText, phoneText;
    private PasswordField passwordText;
    private Label userDataLabel, reloaderDataLabel;
    private ListView<String> listViewUsers = new ListView<>();
    private ArrayList<User> users;
    private ArrayList<String> usersIDls;
    private ObservableList<String> usersID;
    private ObservableList<String> filters, sorting;
    private ComboBox filtersComboBox, sortingComboBox;
    private Label r5DataLabel;

    private ArrayList<ArrayList<Object>> userData = new ArrayList<>();
    private boolean showR5Data = false;

    public TechUserPageView(String tech_id, int width, int height){

        this.tech_id = tech_id;

        techPage = new GridPane();
        scene = new Scene(techPage, width, height);

        Button returnButton = new Button("back to Decision Page");
        returnButton.setOnAction(event ->  ViewController.goToTechDecisionPage(tech_id) );

        Text addUserText = new Text("Add a new user?");

        bankAccountText = new TextField();
        bankAccountText.setPromptText("bankaccount");
        passwordText = new PasswordField();
        passwordText.setPromptText("password");

        Button addScooterButton = new Button("add user");
        addScooterButton.setOnAction(event -> {
            addUser();
            if ( listViewUsers.isVisible() == true )
                users = DbController.getUsers();
                updateUsersListView();
        });

        showUsersButton = new Button("show existing users");
        showUsersButton.setOnAction(event -> {
            if (initUsers || !listViewUsers.isVisible()) {
                showUsers();
            } else {
                hideUserControlVisible();
                setUsersListVisible(false);
            }
        });

        techPage.add(addUserText, 0, 0);
        techPage.add(returnButton, 2,0,1,1);
        techPage.add(bankAccountText, 0, 1, 1,1);
        techPage.add(passwordText, 0, 2, 1,1);
        techPage.add(addScooterButton, 1, 2, 1,1);
        techPage.add(showUsersButton, 0, 3, 1, 1);
    }

    private void addUser() {
        final String regexBank = "\\d{16}";
        if (bankAccountText.getText().matches(regexBank) && !passwordText.getText().equals("")) {
            int id = DbController.registerAnonymeUser(passwordText.getText(), bankAccountText.getText());
            PopUpMessageView.showMessage("The user id "+id+" has been added");
        } else {
            PopUpMessageView.showError("incorrect form :\npassword cannot be null\nbank account must contain 16 digits");
        }
    }

    private void updateUsersListView() {

        usersIDls = new ArrayList<>();

        for (User user : users) {
            String id = "";
            id += user.getId();
            if (user.getNumTrips() != -1){
                id+=(" ("+user.getNumTrips()+")");
            }
            usersIDls.add(id);
        }
        usersID = FXCollections.observableArrayList(usersIDls);

        listViewUsers.setItems(usersID);
        //showR5Data = false;
    }



    private void showUsers() {

        if (initUsers) {
            userDataLabel = new Label();
            reloaderDataLabel = new Label();
            r5DataLabel = new Label();
            users = DbController.getUsers();
            makeUsersListView();
            makeShowUserDataButton();
            makeUpgradeUserButton();
            // makeSortingComboBox();
            makeFiltersComboBox();
            makeDeleteButton();


            initUsers = false;
        }

        updateUsersListView();
        setUsersListVisible(true);
    }

    private void showUpgradeForm() {
        if ( initForm ) {
            makeReloaderForm();
        }

        setFormVisible(true);
    }

    private void showUserData() {
        makeReloaderDataLabel();
        reloaderDataLabel.setVisible(true);
    }

    private void makeReloaderForm() {

        nameText = new TextField();
        surnameText = new TextField();
        adNumText = new TextField();
        adStreetText = new TextField();
        adCityText = new TextField();
        adPostCodeText = new TextField();
        phoneText = new TextField();

        nameText.setPromptText("name");
        surnameText.setPromptText("surname");
        adNumText.setPromptText("number");
        adStreetText.setPromptText("street");
        adCityText.setPromptText("city");
        adPostCodeText.setPromptText("post code");
        phoneText.setPromptText("phone");

        submitButton = new Button("register");
        submitButton.setOnAction(event -> {
            final String regexPhone = "\\d{10,14}";
            final String regexPostCode = "\\d{4}";
            final String regexNum = "\\d+";
            if (    !nameText.getText().equals("") && !surnameText.getText().equals("") &&
                    !adNumText.getText().equals("") && !adCityText.getText().equals("") &&
                    !adPostCodeText.getText().equals("") && !adStreetText.getText().equals("") &&
                    !phoneText.getText().equals("") && phoneText.getText().matches(regexPhone) &&
                    adPostCodeText.getText().matches(regexPostCode) && adNumText.getText().matches(regexNum)) {
                DbController.upgradeUser(selectedUserID, nameText.getText(), surnameText.getText(), phoneText.getText(),
                        adNumText.getText(), adCityText.getText(), adPostCodeText.getText(), adStreetText.getText());
                PopUpMessageView.showMessage("User has been upgraded");
                setFormVisible(false);
                upgradeUserButton.setVisible(false);
                showUserData();
            } else {
                PopUpMessageView.showError("incorrect form :\nplease fill all fields\n" +
                        "phone must contain 10-14 digits\npost code must contain 4 digits");
            }
        });

        techPage.add(nameText, 1, 5);
        techPage.add(surnameText, 1, 6);
        techPage.add(adStreetText, 1, 7);
        techPage.add(adNumText, 1, 8);
        techPage.add(adPostCodeText, 1, 9);
        techPage.add(adCityText, 1, 10);
        techPage.add(phoneText,1,11);
        techPage.add(submitButton, 2,11);
    }

    private void makeDeleteButton() {
        deleteUserButton = new Button("delete user");
        deleteUserButton.setOnAction(event -> {

            if (PopUpMessageView.confirmChoice("Are you sure ? Deleting a user will\n" +
                    "result in the loss of any data associated\n" +
                    "such as trips, complaints, reloads and repairs")){

                DbController.deleteUser(selectedUserID);
                hideUserControlVisible();
                users = DbController.getUsers();
                updateUsersListView();
            }
        });

        techPage.add(deleteUserButton, 0, 6);
    }

    private void makeFiltersComboBox() {
        ArrayList<String> filtersLs = new ArrayList<String>();
        filtersLs.add("no filter");
        filtersLs.add("sort by # trips");
        filtersLs.add("request 5");
        filters = FXCollections.observableArrayList(filtersLs);

        filtersComboBox = new ComboBox(filters);
        filtersComboBox.setPromptText("filter by ...");

        filtersComboBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                showR5Data = false;
                if ( newValue.intValue() == 0){
                    users = DbController.getUsers();
                    updateUsersListView();
                    System.out.println("no filter");
                }
                else if ( newValue.intValue() == 1 ) {
                    System.out.println("Sort by # of trips");
                    users = DbController.usersOrderedByTrips();
                    updateUsersListView();
                }
                else if ( newValue.intValue() == 2){
                    System.out.println("request 5");
                    users = DbController.getUserWithAtLeastTenTripsAndData();
                    showR5Data = true;
                    updateUsersListView();

                }
            }
        });

        techPage.add(filtersComboBox, 1,3);
    }

    /*
    private void makeSortingComboBox() {
        ArrayList<String> sortingsLs = new ArrayList<String>();
        sortingsLs.add("sort by # trips");
        sortingsLs.add("sort by  B");
        sorting = FXCollections.observableArrayList(sortingsLs);

        sortingComboBox = new ComboBox(sorting);
        sortingComboBox.setPromptText("sort by ...");

        sortingComboBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                if ( newValue.intValue() == 0 ) {
                    System.out.println("Sort by # of trips");
                    users = DbController.usersOrderedByTrips();
                    updateUsersListView();
                }
                else if ( newValue.intValue() == 1 )
                    System.out.println("Sort by  B");

            }
        });

        techPage.add(sortingComboBox, 2,3);
    }
    */

    private void makeUpgradeUserButton() {
        upgradeUserButton = new Button();
        upgradeUserButton.setText("upgrade to reloader");
        upgradeUserButton.setOnAction(event -> {
            showUpgradeForm();
        });
        upgradeUserButton.setVisible(false);

        techPage.add(upgradeUserButton, 0,7);

    }

    private void makeShowUserDataButton() {
        showUserDataButton = new Button();
        showUserDataButton.setText("show user data");
        showUserDataButton.setOnAction(event -> {
            showUserData();
        });
        showUserDataButton.setVisible(false);

        techPage.add(showUserDataButton, 0,7);
    }

    private void makeReloaderDataLabel() {
        Reloader reloader = DbController.getReloader(selectedUserID);
        reloaderDataLabel.setText(
                "last name : " + reloader.getLastname() + "\n" +
                "first name : " + reloader.getFirstname() + "\n" +
                "phone : " + reloader.getPhone() + "\n" +
                "address :\n\t-street : " + reloader.getStreet() + "\n" +
                "\t-number : " + reloader.getNumber() + "\n" +
                "\t-post code : " + reloader.getCp() + "\n" +
                "\t-city : " + reloader.getCity() + "\n");
    }

    private void makeR5DataLabel(int index) {

        User user = users.get(index);
        r5DataLabel.setText(
                "  number of trips: " + user.getR5TripNumber() + "\n"
                + "  average time: "+ user.getR5MeanTripLength() + "s\n"
                + "  money spent: " + user.getR5MoneySpent() + "€"
        );

    }

    private void makeUsersListView() {
        listViewUsers.setPrefSize(150, 230); // shows 10 items

        listViewUsers.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != -1) {
                selectNewUser(newValue);
            }
        });

        techPage.add(listViewUsers, 0, 4);
        techPage.add(userDataLabel,0,5);
        techPage.add(reloaderDataLabel, 1,4);
        techPage.add(r5DataLabel, 2, 4);
    }

    private void makeUserDataLabel(Number newValue) {
        userDataLabel.setText(
                "id : " + users.get(newValue.intValue()).getId() + "\n"
                        + "bankaccount : " + users.get(newValue.intValue()).getBankAccount() + "\n");
    }

    private void selectNewUser(Number newValue) {
        showUserDataButton.setVisible(false);
        upgradeUserButton.setVisible(false);
        reloaderDataLabel.setVisible(false);

        makeUserDataLabel(newValue);
        selectedUserID = users.get(newValue.intValue()).getId();

        if ( DbController.isReloader(users.get(newValue.intValue()).getId()) ){
            showUserDataButton.setVisible(true);
        }
        else upgradeUserButton.setVisible(true);
        if (showR5Data){
            makeR5DataLabel(newValue.intValue());
            r5DataLabel.setVisible(true);
        } else {
            r5DataLabel.setVisible(false);
        }

        userDataLabel.setVisible(true);
        deleteUserButton.setVisible(true);
    }

    private void setUsersListVisible(boolean b) {
        listViewUsers.setVisible(b);

        // sortingComboBox.setVisible(b);
        filtersComboBox.setVisible(b);

        if (b) showUsersButton.setText("hide existing users");
        else showUsersButton.setText("show existing users");
    }

    private void setFormVisible(boolean b) {
        nameText.setVisible(b);
        surnameText.setVisible(b);
        adNumText.setVisible(b);
        adStreetText.setVisible(b);
        adCityText.setVisible(b);
        adPostCodeText.setVisible(b);
        phoneText.setVisible(b);
        submitButton.setVisible(b);
    }

    private void hideUserControlVisible() {
        showUserDataButton.setVisible(false);
        deleteUserButton.setVisible(false);
        upgradeUserButton.setVisible(false);
        userDataLabel.setVisible(false);
        reloaderDataLabel.setVisible(false);
    }

    public Scene getScene(){
        return scene;
    }
}
