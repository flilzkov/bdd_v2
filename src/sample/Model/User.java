package sample.Model;

public class User {

    private int id;


    private String password;
    private String bankAccount;
    private int numTrips = -1;

    private double r5TripNumber = -1;
    private double r5MeanTripLength = -1;
    private double r5MoneySpent = -1;


    public double getR5TripNumber() {
        return r5TripNumber;
    }

    public void setR5TripNumber(double r5TripNumber) {
        this.r5TripNumber = r5TripNumber;
    }

    private boolean showR5Data = false;


    public boolean isShowR5Data() {
        return showR5Data;
    }

    public void setShowR5Data(boolean showR5Data) {
        this.showR5Data = showR5Data;
    }

    public User(String id, String password, String bankAccount) {
        this.id = Integer.valueOf(id);
        this.password = password;
        this.bankAccount = bankAccount;
    }

    public User(int id, String password, String bankAccount) {
        this.id = id;
        this.password = password;
        this.bankAccount = bankAccount;
    }

    public int getId() {
        return id;
    }
    public String getPassword() { return password; }
    public String getBankAccount() { return bankAccount; }

    public void setNumTrips(int numTrips) {
        this.numTrips = numTrips;
    }

    public int getNumTrips() {
        return numTrips;
    }


    public double getR5MeanTripLength() {
        return r5MeanTripLength;
    }

    public void setR5MeanTripLength(double r5MeanTripLength) {
        this.r5MeanTripLength = r5MeanTripLength;
    }

    public double getR5MoneySpent() {
        return r5MoneySpent;
    }

    public void setR5MoneySpent(double r5MoneySpent) {
        this.r5MoneySpent = r5MoneySpent;
    }
}
