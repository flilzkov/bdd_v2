package sample.Model;

public class Reload {

    private int scooter;
    private int user;
    private int initialLoad;
    private int finalLoad;
    private float sourceX;
    private float sourceY;
    private float destinationX;
    private float destinationY;
    private String startTime;
    private String endTime;

    public void Reload() {
    }

    public void setScooter(int scooter) {
        this.scooter = scooter;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setInitialLoad(int initialLoad) {
        this.initialLoad = initialLoad;
    }

    public void setFinalLoad(int finalLoad) {
        this.finalLoad = finalLoad;
    }

    public void setSourceX(float sourceX) {
        this.sourceX = sourceX;
    }

    public void setSourceY(float sourceY) {
        this.sourceY = sourceY;
    }

    public void setDestinationX(float destinationX) {
        this.destinationX = destinationX;
    }

    public void setDestinationY(float destinationY) {
        this.destinationY = destinationY;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getScooter() {
        return scooter;
    }

    public int getUser() {
        return user;
    }

    public int getInitialLoad() {
        return initialLoad;
    }

    public int getFinalLoad() {
        return finalLoad;
    }

    public float getSourceX() {
        return sourceX;
    }

    public float getSourceY() {
        return sourceY;
    }

    public float getDestinationX() {
        return destinationX;
    }

    public float getDestinationY() {
        return destinationY;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
