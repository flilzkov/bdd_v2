package sample.Model;

public class Reparation {

    private int scooter;
    private int user;
    private String mechanic;
    private String complainTime;
    private String repairTime;
    private String note = "";

    public void Reparation() {
    }

    public void setScooter(int scooter) {
        this.scooter = scooter;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setMechanic(String mechanic) {
        this.mechanic = mechanic;
    }

    public void setComplainTime(String complainTime) {
        this.complainTime = complainTime;
    }

    public void setRepairTime(String repairTime) {
        this.repairTime = repairTime;
    }

    public int getScooter() {
        return scooter;
    }

    public int getUser() {
        return user;
    }

    public String getMechanic() {
        return mechanic;
    }

    public String getComplainTime() {
        return complainTime;
    }

    public String getRepairTime() {
        return repairTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
