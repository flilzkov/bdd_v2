package sample.Model;

public class Mechanic {

    private String id;
    private String lastname;
    private String firstname;
    private int password;
    private String phone;
    private String city;
    private String cp;
    private String street;
    private String number;
    private String hireDate;
    private String bankaccount;

    public Mechanic(String id, String lastname, String firstname, String password, String phone, String city, String cp, String street, String number, String hireDate, String bankaccount) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.password = Integer.valueOf(password);
        this.phone = phone;
        this.city = city;
        this.cp = cp;
        this.street = street;
        this.number = number;
        this.hireDate = hireDate;
        this.bankaccount = bankaccount;
    }

    public String getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getLastname() { return lastname; }

    public String getFirstname() { return firstname; }

    public int getPassword() { return password; }

    public String getPhone() { return phone; }

    public String getCp() { return cp; }

    public String getStreet() { return street; }

    public String getNumber() { return number; }

    public String getHireDate() { return hireDate; }

    public String getBankaccount() { return bankaccount; }
}
