package sample.Model;

public class Reloader {

    private int id;
    private String lastname;
    private String firstname;
    private String password;
    private String phone;
    private String city;
    private String cp;
    private String street;
    private String number;
    private String bankaccount;

    public Reloader(String id, String lastname, String firstname, String password, String phone, String city, String cp, String street, String number, String bankaccount) {
        this.id = Integer.valueOf(id);
        this.lastname = lastname;
        this.firstname = firstname;
        this.password = password;
        this.phone = phone;
        this.city = city;
        this.cp = cp;
        this.street = street;
        this.number = number;
        this.bankaccount = bankaccount;
    }

    public Reloader(int id, String lastname, String firstname, String password, String phone, String city, String cp, String street, String number, String bankaccount) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.password = password;
        this.phone = phone;
        this.city = city;
        this.cp = cp;
        this.street = street;
        this.number = number;
        this.bankaccount = bankaccount;
    }

    public int getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getCity() {
        return city;
    }

    public String getCp() {
        return cp;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getBankaccount() {
        return bankaccount;
    }
}
