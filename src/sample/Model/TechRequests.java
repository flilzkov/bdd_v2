package sample.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TechRequests {

    public void evolveUser(Connection conn, int user_id, String nom, String prenom, String AdNum, String AdRue, String AdCode, String AdComm) throws SQLException {
        // faire évoluer un utilisateur lambda en utilisateur avec droit de recharge des trottinettes

        final String sql = "INSERT INTO Rechargeur (User_id, Nom, Prénom, AdNum, AdRue, AdComm, AdCode) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, user_id);
            pstmt.setString(2, nom);
            pstmt.setString(3, prenom);
            pstmt.setString(4, AdNum);
            pstmt.setString(5, AdRue);
            pstmt.setString(6, AdComm);
            pstmt.setString(7, AdCode);

            pstmt.executeUpdate();
        }
    }
}

