package sample.Model;

public class Trip {

    private int scooter;
    private int user;
    private float sourceX;
    private float sourceY;
    private float destinationX;
    private float destinationY;
    private String starttime;
    private String endtime;

    public void Trip() {
    }

    public void setScooter(int scooter) {
        this.scooter = scooter;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setSourceX(float sourceX) {
        this.sourceX = sourceX;
    }

    public void setSourceY(float sourceY) {
        this.sourceY = sourceY;
    }

    public void setDestinationX(float destinationX) {
        this.destinationX = destinationX;
    }

    public void setDestinationY(float destinationY) {
        this.destinationY = destinationY;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public int getScooter() {
        return scooter;
    }

    public int getUser() {
        return user;
    }

    public float getSourceX() {
        return sourceX;
    }

    public float getSourceY() {
        return sourceY;
    }

    public float getDestinationX() {
        return destinationX;
    }

    public float getDestinationY() {
        return destinationY;
    }

    public String getStarttime() {
        return starttime;
    }

    public String getEndtime() {
        return endtime;
    }
}
