package sample.Model;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;

public class DataImporter {

    private static final String USER = "user";
    private static final String MECH = "mechanic";
    private static final String ID = "ID";
    private static final String MID = "mechanicID";
    private static final String LAST = "lastname";
    private static final String FIRST = "firstname";
    private static final String PHONE = "phone";
    private static final String ADDRESS = "address";
    private static final String CITY = "city";
    private static final String CP = "cp";
    private static final String STREET = "street";
    private static final String NUM = "number";
    private static final String PWD = "password";
    private static final String ACCOUNT = "bankaccount";
    private static final String HIRE = "hireDate";

    private static DataImporter ourInstance = new DataImporter();

    public static DataImporter getInstance() {
        return ourInstance;
    }

    private DataImporter() {
    }

    /**
     * Returns an array of scooter objects containing all relevant info.
     * @return
     */
    public static ArrayList<Trip> readTripsCsv() {

        String csvFile = "trips.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        ArrayList<Trip> trips = new ArrayList<>();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine(); // This is the headers line
            while ((line = br.readLine()) != null) {

                Trip trip = new Trip();

                String[] trip_info = line.split(csvSplitBy);

                trip.setScooter(Integer.valueOf(trip_info[0]));
                trip.setUser(Integer.valueOf(trip_info[1]));
                trip.setSourceX(Float.valueOf(trip_info[2]));
                trip.setSourceY(Float.valueOf(trip_info[3]));
                trip.setDestinationX(Float.valueOf(trip_info[4]));
                trip.setDestinationY(Float.valueOf(trip_info[5]));
                trip.setStarttime(trip_info[6]);
                trip.setEndtime(trip_info[7]);

                trips.add(trip);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return trips;
    }

    /**
     * Returns an array of Scooter objects containing all relevant info.
     * @return
     */
    public static ArrayList<Scooter> readScootersCsv() {

        String csvFile = "scooters.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        ArrayList<Scooter> scooters = new ArrayList<>();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine(); // This is the headers line
            while ((line = br.readLine()) != null) {

                Scooter scooter = new Scooter();

                String[] scooter_info = line.split(csvSplitBy);

                scooter.setId(Integer.valueOf(scooter_info[0]));
                scooter.setDate(scooter_info[1]);
                scooter.setModel(scooter_info[2]);
                scooter.setPlaint(Boolean.valueOf(scooter_info[3]));
                scooter.setLoad(Integer.valueOf(scooter_info[4]));

                scooters.add(scooter);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return scooters;
    }

    /**
     * Returns an array of reparation objects containing all relevant info.
     * @return
     */
    public static ArrayList<Reparation> readReparationsCsv() {

        String csvFile = "reparations.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        ArrayList<Reparation> reparations = new ArrayList<>();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine(); // This is the headers line
            while ((line = br.readLine()) != null) {

                Reparation reparation = new Reparation();

                String[] reparation_info = line.split(csvSplitBy);

                reparation.setScooter(Integer.valueOf(reparation_info[0]));
                reparation.setUser(Integer.valueOf(reparation_info[1]));
                reparation.setMechanic(reparation_info[2]);
                reparation.setComplainTime(reparation_info[3]);
                reparation.setRepairTime(reparation_info[4]);

                reparations.add(reparation);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return reparations;
    }

    /**
     * Returns an array of reload objects containing all relevant info.
     * @return
     */
    public static ArrayList<Reload> readReloadsCsv() {

        String csvFile = "reloads.csv";
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        ArrayList<Reload> reloads = new ArrayList<>();

        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine(); // This is the headers line
            while ((line = br.readLine()) != null) {

                Reload reload = new Reload();

                String[] reload_info = line.split(csvSplitBy);

                reload.setScooter(Integer.valueOf(reload_info[0]));
                reload.setUser(Integer.valueOf(reload_info[1]));
                reload.setInitialLoad(Integer.valueOf(reload_info[2]));
                reload.setFinalLoad(Integer.valueOf(reload_info[3]));
                reload.setSourceX(Float.valueOf(reload_info[4]));
                reload.setSourceY(Float.valueOf(reload_info[5]));
                reload.setDestinationX(Float.valueOf(reload_info[6]));
                reload.setDestinationY(Float.valueOf(reload_info[7]));
                reload.setStartTime(reload_info[8]);
                reload.setEndTime(reload_info[9]);

                reloads.add(reload);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return reloads;
    }

    /**
     * Récupère le texte entre les balises.
     * Cette méthode est statique pour pouvoir être appelée sans avoir créé d'objet.
     * @param event Event du .xml.
     * @param eventReader Event de lecture du .xml.
     * @return Le texte contenu entre les 2 balises.
     * @throws XMLStreamException Exception à la lecture du xml.
     */
    private static String getCharacterData(XMLEvent event, XMLEventReader eventReader)
            throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }


    /**
     * Returns an array of user objects containing all relevant info.
     * @return
     */
    public static ArrayList<User> readUsersXml() throws FileNotFoundException {
        InputStream in = new FileInputStream(("anonyme_users.xml"));
        ArrayList<User> users = new ArrayList<>();
        // Set header values initial to the empty string
        String id = "";
        String password = "";
        String bankaccount = "";

        try {
            boolean isFeedHeader = true;
            // First create a new XMLInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            // Setup a new eventReader
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // read the XML document
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName()
                            .getLocalPart();
                    switch (localPart) {
                        case USER:
                            if (isFeedHeader) {
                                isFeedHeader = false;
                            }
                            event = eventReader.nextEvent();
                            break;
                        case ID:
                            id = getCharacterData(event, eventReader);
                            break;
                        case PWD:
                            password = getCharacterData(event, eventReader);
                            break;
                        case ACCOUNT:
                            bankaccount = getCharacterData(event, eventReader);
                            break;
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (USER)) {
                        User user = new User(id, password, bankaccount);
                        users.add(user);
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        return users;
    }

    /**
     * Returns an array of user objects containing all relevant info.
     * @return
     */
    public static ArrayList<Reloader> readReloadersXml() throws FileNotFoundException {
        InputStream in = new FileInputStream(("registeredUsers.xml"));
        ArrayList<Reloader> reloaders = new ArrayList<>();
        // Set header values initial to the empty string
        String id = "";
        String lastname = "";
        String firstname = "";
        String password = "";
        String phone = "";
        String city = "";
        String cp = "";
        String street = "";
        String number = "";
        String bankaccount = "";

        try {
            boolean isFeedHeader = true;
            // First create a new XMLInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            // Setup a new eventReader
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // read the XML document
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName()
                            .getLocalPart();
                    switch (localPart) {
                        case USER:
                            if (isFeedHeader) {
                                isFeedHeader = false;
                            }
                            event = eventReader.nextEvent();
                            break;
                        case ID:
                            id = getCharacterData(event, eventReader);
                            break;
                        case PWD:
                            password = getCharacterData(event, eventReader);
                            break;
                        case ACCOUNT:
                            bankaccount = getCharacterData(event, eventReader);
                            break;
                        case LAST:
                            lastname = getCharacterData(event, eventReader);
                            break;
                        case FIRST:
                            firstname = getCharacterData(event, eventReader);
                            break;
                        case PHONE:
                            phone = getCharacterData(event, eventReader);
                            break;
                        case CITY:
                            city = getCharacterData(event, eventReader);
                            break;
                        case CP:
                            cp = getCharacterData(event, eventReader);
                            break;
                        case STREET:
                            street = getCharacterData(event, eventReader);
                            break;
                        case NUM:
                            number = getCharacterData(event, eventReader);
                            break;
                        case ADDRESS:
                            break;
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (USER)) {
                        Reloader reloader = new Reloader(id, lastname, firstname, password, phone, city, cp, street, number, bankaccount);
                        reloaders.add(reloader);
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        return reloaders;
    }

    /**
     * Returns an array of user objects containing all relevant info.
     * @return
     */
    public static ArrayList<Mechanic> readMechanicsXml() throws FileNotFoundException {
        InputStream in = new FileInputStream(("mecaniciens.xml"));
        ArrayList<Mechanic> mechanics = new ArrayList<>();
        // Set header values initial to the empty string
        String id = "";
        String lastname = "";
        String firstname = "";
        String password = "";
        String phone = "";
        String city = "";
        String cp = "";
        String street = "";
        String number = "";
        String bankaccount = "";
        String hireDate = "";

        try {
            boolean isFeedHeader = true;
            // First create a new XMLInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            // Setup a new eventReader
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // read the XML document
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName()
                            .getLocalPart();
                    switch (localPart) {
                        case MECH:
                            if (isFeedHeader) {
                                isFeedHeader = false;
                            }
                            event = eventReader.nextEvent();
                            break;
                        case MID:
                            id = getCharacterData(event, eventReader);
                            break;
                        case PWD:
                            password = getCharacterData(event, eventReader);
                            break;
                        case ACCOUNT:
                            bankaccount = getCharacterData(event, eventReader);
                            break;
                        case LAST:
                            lastname = getCharacterData(event, eventReader);
                            break;
                        case FIRST:
                            firstname = getCharacterData(event, eventReader);
                            break;
                        case PHONE:
                            phone = getCharacterData(event, eventReader);
                            break;
                        case CITY:
                            city = getCharacterData(event, eventReader);
                            break;
                        case CP:
                            cp = getCharacterData(event, eventReader);
                            break;
                        case STREET:
                            street = getCharacterData(event, eventReader);
                            break;
                        case NUM:
                            number = getCharacterData(event, eventReader);
                            break;
                        case ADDRESS:
                            break;
                        case HIRE:
                            hireDate = getCharacterData(event, eventReader);
                            break;
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (MECH)) {
                        Mechanic mechanic = new Mechanic(id, lastname, firstname, password, phone, city, cp, street, number, hireDate, bankaccount);
                        mechanics.add(mechanic);
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        return mechanics;
    }
}

