package sample.Model;

public class Scooter {

    private int id;
    private int numReparations = -1;
    private Double dist = -1.0;
    private String date = "today";
    private String model = "model1";
    private boolean plaint = false;
    private int load;
    private int available;
    private double posX;
    private double posY;
    private int trips = -1;

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getModel() {
        return model;
    }

    public int getLoad() {
        return load;
    }

    public boolean getPlaint() {
        return plaint;
    }

    public int getAvailable() {
        return available;
    }

    public void Scooter() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPlaint(boolean plaint) {
        this.plaint = plaint;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public void setNumReparations(int numReparations) {
        this.numReparations = numReparations;
    }

    public int getNumReparations() {
        return numReparations;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public double getDist() {
        return dist;
    }

    public void setTrips(int trips) {
        this.trips = trips;
    }

    public int getTrips() {
        return trips;
    }
}
