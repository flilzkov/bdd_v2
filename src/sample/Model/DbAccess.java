package sample.Model;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;


public class DbAccess {

    private static Connection connection;


    public static Connection getConnection(){


        String dbName="PAF";
        String userName = "infoh303";
        String password = "";
        String url = "jdbc:mysql://localhost:3306/" + dbName + "?autoReconnect=true&useSSL=false&rewriteBatchedStatements=true";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(url, userName, password);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }

    /**
     * Récupère la liste des trottinettes 'available' dans le base de données.
     * @return ArrayList<Scooter>
     * @throws SQLException /
     */

    public static ArrayList<Scooter> getAvailableScooters() throws SQLException {

        ArrayList<Scooter> scooters = new ArrayList<>();

        String sql = "SELECT * FROM Scooters s WHERE s.available = 1;";
        Connection conn = DbAccess.getConnection();
        Statement statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while (res.next()) {
            Scooter scooter = new Scooter();
            scooter.setId(res.getInt("sid"));
            scooter.setLoad(res.getInt("battery"));
            scooter.setModel(res.getString("model"));
            scooter.setDate(res.getString("dateService"));
            scooter.setPlaint(res.getBoolean("plaint"));
            scooter.setPosX(res.getDouble("posX"));
            scooter.setPosY(res.getDouble("posY"));
            scooters.add(scooter);
        }
        return scooters;
    }

    public static ArrayList<Scooter> getScooters() throws SQLException {
        ArrayList<Scooter> scooters = new ArrayList<Scooter>();

        // to fill with proper SQL request
        String sql = "SELECT * FROM Scooters";
        Connection conn = DbAccess.getConnection();
        Statement statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        createFilteredScooterList(scooters, res);
        return scooters;
    }

    public static boolean checkIdMatchPasswordUser(int id, String password){
        int nb = 0;
        try {
            Connection conn = DbAccess.getConnection();

            String sql = "SELECT COUNT(*) FROM AnonymeUsers u WHERE u.uid = ? AND u.password = ?;";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1,id);
            preparedStmt.setString(2, password);

            ResultSet res = preparedStmt.executeQuery();
            if(res.next()){
                nb = res.getInt(1);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in checkIdMatchPasswordUser!");
            System.err.println(e.getMessage());
        }
        if(nb!=1){
            System.out.println("!! L'utilisateur n°"+ id+ " n'est pas associe au mdp "+password);
        } else {
            System.out.println("OK. L'utilisateur n°"+ id+ " est associe au mdp "+password);
        }
        return(nb==1);
    }

    /**
     * Méthode qui permet d'ajouter un utilisateur anonyme à la base de données.
     * L'ID est ici spécifié (utile pour l'insertion des données depuis le fichier XML).
     * @param uid id du user
     * @param password mot de passe
     * @param bankaccount n° compte en banque
     */

    public static void addAnonymeUser(Connection conn, int uid, String password, String bankaccount){

        try {
            String sql = "INSERT INTO AnonymeUsers(uid,password, bankaccount)" +
                    "VALUES(?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1,uid);
            preparedStmt.setString(2, password);
            preparedStmt.setString(3, bankaccount);
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    /**
     * Méthode qui créé un nouvel utilisateur dans la base de données et
     * renvoie l'UID de l'utilisateur qui vient d'être créé.
     * L'UID est auto-incrémenté.
     * @param password - mot de passe
     * @param bankaccount - compte en banque
     * @return l'ID auto-incrémenté du User qui vient d'être créé.
     */

    public static int addAnonymeUser(String password, String bankaccount){

        int autoIncrementID = -1;

        try {
            Connection conn = DbAccess.getConnection();

            String sql = "INSERT INTO AnonymeUsers(password, bankaccount)" +
                    "VALUES(?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            preparedStmt.setString(1, password);
            preparedStmt.setString(2, bankaccount);
            preparedStmt.executeUpdate();
            ResultSet res = preparedStmt.getGeneratedKeys();
            if (res.next()) {
                autoIncrementID = res.getInt(1);
                System.out.println("Bienvenu ! Votre id est "+autoIncrementID);
            }
            conn.close();

        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
        return autoIncrementID;
    }

    /**
     * Insertion de plusieurs utilisateurs, ne donc nécéssitant qu'une connexion à la Db.
     * @param users, ArrayList d'objet user.
     * @throws /SQLException
     */
    public static void insertManyAnonymeUser(ArrayList<User> users) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {
            for (User user : users) {
                addAnonymeUser(conn, user.getId(), user.getPassword(), user.getBankAccount());
            }
            System.out.println("Tous les utilisateurs anonymes ont été ajoutés.");
        }
    }
    public static void insertManyScooter(ArrayList<Scooter> scooters) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {
            for (Scooter s : scooters) {
                addScooter(conn, s.getId(), s.getLoad(), s.getDate(), s.getModel(), s.getPlaint());
            }
            System.out.println("Toutes les trottinettes ont été ajoutées.");

        }
    }

    public static void addScooter(int id, int load, String dateService, String model, boolean plaint){

        try {
            Connection conn = DbAccess.getConnection();

            String sql = "INSERT INTO Scooters(sid,battery,dateService,model,plaint)" +
                    "VALUES(?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, id);
            preparedStmt.setInt(2, load);
            preparedStmt.setString(3, dateService);
            preparedStmt.setString(4, model);
            preparedStmt.setBoolean(5, plaint);
            preparedStmt.execute();
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public static void addScooter(Connection conn, int id, int load, String dateService, String model, boolean plaint){

        try {
            String sql = "INSERT INTO Scooters(sid,battery,dateService,model,plaint)" +
                    "VALUES(?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, id);
            preparedStmt.setInt(2, load);
            preparedStmt.setString(3, dateService);
            preparedStmt.setString(4, model);
            preparedStmt.setBoolean(5, plaint);
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public static void addMechanic(Connection conn, String id, String lastname, String firstname, int password,
                                   String phone, String city, String cp, String street,
                                   String number, String hireDate, String bankaccount){

        try {
            String sql = "INSERT INTO Mechanics VALUES(?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, id);
            preparedStmt.setString(2, lastname);
            preparedStmt.setString(3, firstname);
            preparedStmt.setInt(4, password);
            preparedStmt.setString(5, phone);
            preparedStmt.setString(6, city);
            preparedStmt.setString(7, cp);
            preparedStmt.setString(8, street);
            preparedStmt.setString(9, number);
            preparedStmt.setString(10, hireDate);
            preparedStmt.setString(11, bankaccount);
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    /**
     * Ajout de plusieurs mécaniciens à la base de données.
     */
    public static void insertManyMechanics(ArrayList<Mechanic> mechanics) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {
            for (Mechanic m : mechanics) {
               addMechanic(conn, m.getId(), m.getLastname(), m.getFirstname(), m.getPassword(),
                       m.getPhone(), m.getCity(), m.getCp(), m.getStreet(), m.getNumber(),
                       m.getHireDate(), m.getBankaccount());
            }
            System.out.println("Tous les mécaniciens ont été ajoutés!");
        }
    }

    /**
     * Ajoute une plainte dans la Db.
     * Avant l'ajout, vérification que l'utilisateur n'a pas déjà introduit une plainte non-résolue
     * pour cette trottinette.
     * La fonction renvoie 'true' si l'insertion a été faite et 'false' dans les autres cas.
     * @param scooterID
     * @param userID
     * @return
     */
    public static boolean addPlaint(int scooterID, int userID) {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String strDate = dateFormat.format(date);

        /* Vérifions que l'utilisateur n'a pas déjà introduit de plainte non-résolu
        sur cette trottinette. */
        int nb = 0;
        try {
            Connection conn = DbAccess.getConnection();

            String sql = "SELECT COUNT(*) FROM Plaints p WHERE p.sid = ? AND p.uid = ? AND p.state = 1";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1,scooterID);
            preparedStmt.setInt(2, userID);

            ResultSet res = preparedStmt.executeQuery();
            if(res.next()){
                nb = res.getInt(1);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in addPlaint!");
            System.err.println(e.getMessage());
        }
        if(nb==1){
            System.out.println("!! Vous avez déjà introduit une plainte pour cette trottinette. Un technicien s'en occupe.");
            return false;
        }

        try {
            Connection conn = DbAccess.getConnection();

            String sql1 = "INSERT INTO Plaints(sid,date,uid)" +
                    "VALUES(?,?,?)";

            PreparedStatement preparedStmt1 = conn.prepareStatement(sql1);
            preparedStmt1.setInt(1, scooterID);
            preparedStmt1.setString(2, strDate);
            preparedStmt1.setInt(3, userID);
            preparedStmt1.execute();
            System.out.println("user :" + userID + "added a plaint on scooter :" + scooterID);

            String sql2 = "UPDATE Scooters s SET plaint = 1 where s.sid = ?;";
            PreparedStatement preparedStmt2 = conn.prepareStatement(sql2);
            preparedStmt2.setInt(1, scooterID);
            preparedStmt2.execute();
            conn.close();
            return true;
        } catch (Exception e) {
            System.err.println("Got an exception in addPlaint!");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public static void addReparation(Connection conn, int scooter, int user, String mechanic, String complainTime, String repairTime){
        try {
            String sql = "INSERT INTO Reparations VALUES(?,?,?,?,?,'')";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, scooter);
            preparedStmt.setInt(2, user);
            preparedStmt.setString(3, mechanic);
            preparedStmt.setString(4, complainTime);
            preparedStmt.setString(5, repairTime);
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Got an exception! "+Integer.toString(user));
            System.err.println(e.getMessage());
        }
    }

    public static void insertManyReparations(ArrayList<Reparation> reparations) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {

            for (Reparation r : reparations) {
                addReparation(conn, r.getScooter(), r.getUser(), r.getMechanic(), r.getComplainTime(), r.getRepairTime());
            }
            System.out.println("Toutes les réparations ont été ajoutées.");
        }
    }


    public static void addReloader(Connection conn, int ID, String lastname, String firstname, String password, String phone,
                                   String city, String cp, String street, String number, String bankaccount) {
        addAnonymeUser(conn, ID, password, bankaccount);

        try {
            String sql = "INSERT INTO Users VALUES(?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, ID);
            preparedStmt.setString(2, lastname);
            preparedStmt.setString(3, firstname);
            preparedStmt.setString(4, phone);
            preparedStmt.setString(5, city);
            preparedStmt.setString(6, cp);
            preparedStmt.setString(7, street);
            preparedStmt.setString(8, number);
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Got an exception! " + Integer.toString(ID));
            System.err.println(e.getMessage());
        }
    }

    public static int addReloader(String lastname, String firstname, String password, String phone,
                                   String city, String cp, String street, String number, String bankaccount){
        int id = addAnonymeUser(password, bankaccount);

        Connection conn = DbAccess.getConnection();

        try {
            String sql = "INSERT INTO Users VALUES(?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, id);
            preparedStmt.setString(2, lastname);
            preparedStmt.setString(3, firstname);
            preparedStmt.setString(4, phone);
            preparedStmt.setString(5, city);
            preparedStmt.setString(6, cp);
            preparedStmt.setString(7, street);
            preparedStmt.setString(8, number);
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Got an exception! "+Integer.toString(id));
            System.err.println(e.getMessage());
        }
        return id;
    }

    public static void insertManyReloaders(ArrayList<Reloader> reloaders) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {
            for (Reloader r : reloaders) {
                addReloader(conn, r.getId(), r.getLastname(), r.getFirstname(), r.getPassword(),
                        r.getPhone(), r.getCity(), r.getCp(), r.getStreet(), r.getNumber(), r.getBankaccount());
            }
            System.out.println("Tous les rechargeurs ont été ajoutés.");
        }
    }

    public static void insertManyTrips(ArrayList<Trip> trips) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {


            String sql = "INSERT INTO Trips VALUES(?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);

            for (Trip t : trips){
                preparedStmt.setInt(1, t.getScooter());
                preparedStmt.setInt(2, t.getUser());
                preparedStmt.setFloat(3, t.getSourceX());
                preparedStmt.setFloat(4, t.getSourceY());
                preparedStmt.setFloat(5, t.getDestinationX());
                preparedStmt.setFloat(6, t.getDestinationY());
                preparedStmt.setString(7, t.getStarttime());
                preparedStmt.setString(8, t.getEndtime());
                preparedStmt.addBatch();
            }
            long start = System.currentTimeMillis();
            preparedStmt.executeBatch();
            long end = System.currentTimeMillis();
            System.out.println(end-start);

            preparedStmt.close();
            conn.close();
            System.out.println("Tous les trajets ont été ajoutés.");
        }
    }

    public static void insertManyReloads(ArrayList<Reload> reloads) throws SQLException {
        try (Connection conn = DbAccess.getConnection()) {

            final String sql = "INSERT INTO Reloads VALUES(?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);

            for (Reload r : reloads) {

                preparedStmt.setInt(1, r.getScooter());
                preparedStmt.setInt(2, r.getUser());
                preparedStmt.setInt(3, r.getInitialLoad());
                preparedStmt.setInt(4, r.getFinalLoad());
                preparedStmt.setFloat(5, r.getSourceX());
                preparedStmt.setFloat(6, r.getSourceY());
                preparedStmt.setFloat(7, r.getDestinationX());
                preparedStmt.setFloat(8, r.getDestinationY());
                preparedStmt.setString(9, r.getStartTime());
                preparedStmt.setString(10, r.getEndTime());
                preparedStmt.addBatch();
            }

            long start = System.currentTimeMillis();
            preparedStmt.executeBatch();
            long end = System.currentTimeMillis();
            System.out.println(end-start);
            System.out.println("Toutes les recharges ont été ajoutées.");

        }
    }

    //Scooter devient unavailable et position devient indéterminée
    public static void rentScooter(int scooterID) {
        try {
            Connection conn = DbAccess.getConnection();

            String sql = "UPDATE Scooters s SET available=0 where s.sid = ?;";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, scooterID);
            preparedStmt.execute();
            conn.close();
            updateNoLocalisation(scooterID);
        } catch (Exception e) {
            System.err.println("Got an exception in rentScooter!");
            System.err.println(e.getMessage());
        }
        System.out.println("Scooter n° "+scooterID+" rent !");
    }

    /**
     * Insert un nouveau trajet dans la table 'trips' pour l'userID avec le scooterID et pour
     * l'heure actuelle.
     * @param scooterID
     * @param userID
     */
    public static void startTrip(int scooterID, int userID) {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String strDate = dateFormat.format(date);
        System.out.println("startTrip OK");

        try {
            Connection conn = DbAccess.getConnection();
            ArrayList<Double> posScooter = getLocalisationScooter(scooterID);

            String sql = "INSERT INTO Trips(scooter,user, sourcex, sourcey, starttime)" +
                    "VALUES(?,?,"+posScooter.get(0)+","+posScooter.get(1)+",?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1,scooterID);
            preparedStmt.setInt(2, userID);
            preparedStmt.setString(3, strDate);
            preparedStmt.executeUpdate();
            System.out.println("Utilisateur "+userID+" a commencé un trajet avec trottinette "+scooterID);
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    //méthode qui sert à ramener une trottinette ET vérfie si le user a emprunté cette trottinette
    public static boolean endTrip(int scooterID, int userID) {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String strDate = dateFormat.format(date);
        Random rX = new Random();
        Random rY = new Random();
        double randomPosX = 4.36+ (4.378 - 4.36) * rX.nextDouble();
        double randomPosY = 50.82 + ( 50.879 - 50.82) * rY.nextDouble();
        updateLocalisation(scooterID, randomPosX, randomPosY);
        System.out.println("Trottinette"+scooterID+"est maintenant en "+randomPosX+" ; "+randomPosY);
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "UPDATE Trips s set s.destinationx ="+randomPosX
                    +", s.destinationy = " +randomPosY+
                    ",endtime = ? WHERE s.scooter=? AND s.user=? AND endtime IS NULL;\n";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(2,scooterID);
            preparedStmt.setInt(3, userID);
            preparedStmt.setString(1, strDate);
            int row = preparedStmt.executeUpdate();
            conn.close();
            if(row==0){
                return false;
            } else {
                System.out.println("ROW: "+row+" Utilisateur "+userID+" a rendu la trottinette "+scooterID);
                return true;
            }
        } catch (Exception e) {
            System.err.println("Got an exception! in endTrip");
            System.err.println(e.getMessage());
            return false;
        }
    }

    //méthode qui sert à mettre le scooter available et modifier sa position sa batterie (apres trip ou reload)
    public static void returnScooter(int scooterID, int load){
        try {
            Connection conn = DbAccess.getConnection();

            String sql = "UPDATE Scooters s SET available=1, battery = ? where s.sid = ?;";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, load);
            preparedStmt.setInt(2, scooterID);
            preparedStmt.execute();
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in returnScooter!");
            System.err.println(e.getMessage());
        }
        System.out.println("Scooter n° "+scooterID+" returned !");
    }

    public static ArrayList<Object> getDataFromCurrentRent(int userId) {
        /* Renvoie une liste avec les informations courrantes : userID, scooterID, dateDebut, prix.
           Si pas de location, return une array vide. */
        ArrayList<Object> data = new ArrayList<>();
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String dateEnd = dateFormat.format(date);

        try {
            Connection conn = DbAccess.getConnection();
            String user = String.valueOf(userId);
            String sql = "SELECT * FROM Trips t WHERE t.user = '"+user+"' AND t.endtime IS NULL;";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            if (!res.next()) {
                //then there are no rows.
                System.out.println("No records found");
                return null;
            } else {
                data.add(res.getInt("user"));
                data.add(res.getInt("scooter"));
                data.add(res.getString("starttime"));
                data.add(calculatePrice(res.getString("starttime"),dateEnd));
                conn.close();
                return data;
            }

        } catch (Exception e) {
            System.err.println("Got an exception in getDataFromCurrentRent!");
            System.err.println(e.getMessage());
            return null;
        }


    }

    public static ArrayList<Object> getDataFromCurrentReload(int userId) {
        /* Renvoie une liste avec les informations courrantes : userId, scooterID, charge initiale et date début.
           Si pas de recharge, return une array vide. */
        ArrayList<Object> data = new ArrayList<>();
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String dateEnd = dateFormat.format(date);

        try {
            Connection conn = DbAccess.getConnection();
            String user = String.valueOf(userId);
            String sql = "SELECT * FROM Reloads r WHERE r.user = '"+user+"' AND r.endtime IS NULL;";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            if (!res.next()) {
                //then there are no rows.
                System.out.println("No records found");
                return null;
            } else {
                data.add(res.getInt("user"));
                data.add(res.getInt("scooter"));
                data.add(res.getInt("initialload"));
                data.add(res.getString("starttime"));
                conn.close();
                return data;
            }

        } catch (Exception e) {
            System.err.println("Got an exception in getDataFromCurrentReloadt!");
            System.err.println(e.getMessage());
            return null;
        }


    }

    public static float calculatePrice(String stringBegin, String stringEnd) {
        float diff = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date dateBegin;
        Date dateEnd;
        try {
            dateBegin = formatter.parse(stringBegin);
            dateEnd = formatter.parse(stringEnd);
            diff = (dateEnd.getTime()-dateBegin.getTime())/1000; //temps en secondes
            if(diff<60){ //moins d'une minute sur la trottinette
                System.out.println("Gratuit");
                return 0+1;
            } else if(diff >= 24*3600){ //plus d'une journée sur la trottinette
                System.out.println("Plus d'une journée sur la trottinette: 36€/jour");
                int diffDays = (int)(diff / (24 * 60 * 60));
                System.out.println("Temps passé: "+diffDays +" jours");
                return diffDays*36+1;
            } else if (diff >= 3600){ //plus d'une heure sur la trottinette
                System.out.println("Plus d'une heure sur la trottinette: 6.5€/heure");
                int diffhours = (int)(diff /(60 * 60));
                System.out.println("Temps passé: "+diffhours+" heures");
                return (float) (diffhours*6.5+1);
            } else {
                int diffmin = (int)(diff /(60));
                System.out.println("Moins d'une heure sur la trottinette: 0.15€/minute");
                System.out.println("Temps passé: "+diffmin+" minutes");
                return (float) (diffmin*0.15+1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diff;
    }
    public static boolean hasUnresolvedComplaint(int scooterID){
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "SELECT * FROM Scooters s WHERE s.sid = '"+scooterID+"' AND s.plaint = '1';";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            return (res.next());

        } catch (Exception e) {
            System.err.println("Got an exception in hasUnresolvedComplaint!");
            System.err.println(e.getMessage());
            return false;
        }

    }
    public static boolean isUnload(int scooterID){
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "SELECT * FROM Scooters s WHERE s.sid = '"+scooterID+"' AND s.battery = '0';";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            return (res.next());

        } catch (Exception e) {
            System.err.println("Got an exception in isUnload!");
            System.err.println(e.getMessage());
            return false;
        }

    }

    public static int getScooterStatus(int scooterID){

        try {
            Connection conn = DbAccess.getConnection();
            String sql = "SELECT Available FROM Scooters s WHERE s.sid = '"+scooterID+"'";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            if (res.next()) {

                return res.getInt("available");
            }

        } catch (Exception e) {
            System.err.println("Got an exception in getScooterStatus!");
            System.err.println(e.getMessage());
        }
        return -1;
    }

    public static int getScooterLoad(int scooterID){

        try {
            Connection conn = DbAccess.getConnection();
            String sql = "SELECT Battery FROM Scooters s WHERE s.sid = '"+scooterID+"'";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            if (res.next()) {
                return res.getInt("battery");
            }

        } catch (Exception e) {
            System.err.println("Got an exception in getScooterLoad!");
            System.err.println(e.getMessage());
        }
        return -1;
    }

    public static void changeScooterStatus(int selectedScooterID) {
        try {
            Connection conn = DbAccess.getConnection();

            int status;

            String sql = "SELECT available FROM Scooters s WHERE s.sid = '"+selectedScooterID+"'";

            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet res = pstmt.executeQuery(sql);
            res.next();
            status = res.getInt("available");
            if (status == 1) status = 0;
            else status = 1;

            sql = "UPDATE Scooters SET available = ? WHERE sid = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, status);
            pstmt.setInt(2, selectedScooterID);

            pstmt.executeUpdate();
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
    /* Méthode qui vérifie si le userID est un utilisateur rechargeur.
     * Attention, ne vérifie pas si l'userID existe ou si le mdp est correct!
     */
    public static boolean isReloader(int userID){
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "SELECT * FROM Users u WHERE u.user = '"+userID+"';";
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            return (res.next());

        } catch (Exception e) {
            System.err.println("Got an exception in isReloader!");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public static boolean checkIdMatchPasswordMechanic(String mechanicID, String password){
        int nb = 0;
        try {
            Connection conn = DbAccess.getConnection();

            String sql = "SELECT COUNT(*) FROM Mechanics m WHERE m.mid = ? AND m.password = ?;";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1,mechanicID);
            preparedStmt.setString(2, password);

            ResultSet res = preparedStmt.executeQuery();
            if(res.next()){
                nb = res.getInt(1);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in checkIdMatchPasswordMechanic!");
            System.err.println(e.getMessage());
        }
        if(nb!=1){
            System.out.println("!! Technicien n°"+ mechanicID+ " n'est pas associe au mdp "+password);
        } else {
            System.out.println("OK. Technicien n°"+ mechanicID+ " est associe au mdp "+password);
        }
        return(nb==1);
    }

    /* Pour un utilisateur donné, renvoie l'historique de tous les trajets effectués.*/

    public static ArrayList<Trip> getTripHistoryUser(int userID) throws SQLException {

        ArrayList<Trip> trips = new ArrayList<>();

        String sql = "SELECT * FROM Trips t WHERE t.user ='"+userID+"' ORDER BY t.endtime;";
        Connection conn = DbAccess.getConnection();
        Statement statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while (res.next()) {
            Trip trip = new Trip();
            trip.setUser(res.getInt("user"));
            trip.setScooter(res.getInt("scooter"));
            trip.setDestinationX(res.getFloat("destinationX"));
            trip.setDestinationY(res.getFloat("destinationY"));
            trip.setSourceX(res.getFloat("sourcex"));
            trip.setSourceY(res.getFloat("sourceY"));
            trip.setStarttime(res.getString("starttime"));
            trip.setEndtime(res.getString("endtime"));
            trips.add(trip);
        }
        return trips;

    }

    public static void startReload(int scooterID, int userID) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        java.sql.Date actualTime = new java.sql.Date(new java.util.Date().getTime());
        String startTime = format.format(actualTime);
        String sql = "INSERT INTO Reloads VALUES(?,?,?,4,0,0,NULL,NULL,?,NULL)";
        try {
            Connection conn = DbAccess.getConnection();
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, scooterID);
            preparedStmt.setInt(2, userID);
            preparedStmt.setInt(3, getScooterLoad(scooterID));
            preparedStmt.setString(4, startTime);
            preparedStmt.executeUpdate();
            updateNoLocalisation(scooterID);
        } catch (Exception e) {
            System.err.println("Got an exception in addReload! " + Integer.toString(userID));
            System.err.println(e.getMessage());
        }
        return;

    }

    public static boolean endReload(int scooterID, int userID) {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String strDate = dateFormat.format(date);
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "UPDATE Reloads r set r.destinationx = 1, r.destinationy = 1, " +
                    "endtime = ? WHERE r.scooter=? AND r.user=? AND endtime IS NULL;\n";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, strDate);
            preparedStmt.setInt(2,scooterID);
            preparedStmt.setInt(3, userID);

            int row = preparedStmt.executeUpdate();
            conn.close();
            if(row==0){
                return false;
            } else {
                System.out.println("ROW: "+row+" Utilisateur "+userID+" a rechargé la trottinette "+scooterID);
                return true;
            }
        } catch (Exception e) {
            System.err.println("Got an exception! in endReload");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public static boolean checkScooterExists(int id) throws SQLException {
        final String sql = "SELECT * FROM Scooters s WHERE s.sid = '"+id+"'";

        Connection conn = DbAccess.getConnection();
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        ResultSet res = preparedStmt.executeQuery();

        if (!res.next()) return false;
        else return true;
    }

    public static void deleteScooter(int selectedScooterID) {
        String sql1 = "DELETE FROM Trips WHERE scooter ="+selectedScooterID+";";
        String sql2 = "DELETE FROM Reloads WHERE scooter ="+selectedScooterID+";";
        String sql3 = "DELETE FROM Reparations WHERE scooter ="+selectedScooterID+";";
        String sql4 = "DELETE FROM Plaints WHERE sid ="+selectedScooterID+";";
        String sql = "DELETE FROM Scooters WHERE sid ="+selectedScooterID+";";

        Connection conn = DbAccess.getConnection();
        try {
            PreparedStatement pstmt1 = conn.prepareStatement(sql1);
            PreparedStatement pstmt2 = conn.prepareStatement(sql2);
            PreparedStatement pstmt3 = conn.prepareStatement(sql3);
            PreparedStatement pstmt4 = conn.prepareStatement(sql4);

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt1.executeUpdate();
            pstmt2.executeUpdate();
            pstmt3.executeUpdate();
            pstmt4.executeUpdate();
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.err.println("Got an exception in deleteScooter!");
            System.err.println(e.getMessage());
        }
    }

    public static void treatPlaint(int scooterID, String techID, String note) {

        /* Récupération de la date actuelle sous forme de String */
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String repairTime = dateFormat.format(date);

        int userID = -1;
        String complainTime = "";

        String sql = "SELECT * FROM Plaints p WHERE sid ='"+scooterID+"'AND p.state=1;";
        /* Toutes les plaintes non-résolues concernant scooterID. */

        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();

            while(res.next()){
                /* Assure que userID sera le dernier user qui s'est plaint. */
                System.out.println("user: "+res.getInt("uid"));
                userID = res.getInt("uid");
                complainTime = res.getString("date");
            }
        } catch (Exception e) {
            System.err.println("Got an exception in treatPlaint! (step 1)");
            System.err.println(e.getMessage());
        }
        if (userID==-1) System.out.println("Pas de plaintes pour cette trottinette");
        else {
            /* Créer une réparation associée à la dernière plainte enregistrée */

            String sql2 = "INSERT INTO Reparations VALUES (?,?,?,?,?,?)";

            try {
                PreparedStatement pstmt2 = conn.prepareStatement(sql2);
                pstmt2.setInt(1, scooterID);
                pstmt2.setInt(2, userID);
                pstmt2.setString(3, techID);
                pstmt2.setString(4, complainTime);
                pstmt2.setString(5, repairTime);
                pstmt2.setString(6, note);

                pstmt2.executeUpdate();
            } catch (Exception e) {
                System.err.println("Got an exception in treatPlaint! (step 2)");
                System.err.println(e.getMessage());
            }

            String sql3 = "UPDATE Scooters SET available = 1, plaint = 0 WHERE sid ="+scooterID+";";
            /* Le scooter est à nouveau disponible et est réparé. */
            String sql4 = "UPDATE Plaints SET state=0 WHERE state=1 AND sid ="+scooterID+";";
            /* Toutes les plaintes concernant ce scooter sont résolues. */

            try {
                PreparedStatement pstmt3 = conn.prepareStatement(sql3);
                PreparedStatement pstmt4 = conn.prepareStatement(sql4);

                pstmt3.executeUpdate();
                pstmt4.executeUpdate();
            } catch (Exception e) {
                System.err.println("Got an exception in treatPlaint! (step 3)");
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * Méthode qui renvoie la liste des trottinettes ayant reçues au moins 10 plaintes
     * @return ArrayList d'entier qui représentent les scooters.
     */
    public static ArrayList<Scooter> getScooterWithAtLeastTenPlaints(){
    ArrayList<Scooter> scooters = new ArrayList<>();
    String sql ="SELECT * FROM Scooters s, " +
            "(SELECT p.sid, count(*) c FROM Plaints p GROUP BY p.sid) t WHERE s.sid = t.sid AND t.c>=10";
    Connection conn = getConnection();
        try {
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        ResultSet res = preparedStmt.executeQuery();
            createFilteredScooterList(scooters, res);
            if(scooters.size()==0) System.out.println("Aucune trottinette n'a reçu plus de 10 plaintes");
    } catch (Exception e) {
        System.err.println("Got an exception in getScooterWithAtLeastTenPlaints!");
        System.err.println(e.getMessage());
    }

        return scooters;
    }
    public static ArrayList<User> usersOrderedByTrips(){
        ArrayList<User> userTripList = new ArrayList<>();

        String sql = "SELECT * FROM AnonymeUsers us LEFT JOIN (SELECT u.user, count(*) c FROM Trips u " +
                    "GROUP BY u.user) t ON us.uid = t.user ORDER BY c DESC;";
        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();
            while (res.next()) {
                User user = new User(res.getInt("uid"), res.getString("password"),
                        res.getString("bankaccount"));
                user.setNumTrips(res.getInt("c"));
                userTripList.add(user);
            }
            if(userTripList.size()==0) System.out.println("Aucun trajet effectué");
        } catch (Exception e) {
            System.err.println("Got an exception in usersOrderedByTrips!");
            System.err.println(e.getMessage());
        }
        return userTripList;
    }

    public static ArrayList<Scooter> getScootersWithUnresolvedComplaints(){
        ArrayList<Scooter> scooters = new ArrayList<>();

        String sql ="SELECT * FROM Scooters s WHERE exists " +
                "(SELECT p.sid  FROM Plaints p WHERE s.sid = p.sid AND p.state = 1)";
        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();
            createFilteredScooterList(scooters, res);
            if(scooters.size()==0) System.out.println("Aucune trottinette n'a de plainte ouverte");
        } catch (Exception e) {
            System.err.println("Got an exception in getScooterWithUnresovedComplaints!");
            System.err.println(e.getMessage());
        }

        return scooters;
    }

    public static ArrayList<Reparation> getRepairs(int selectedScooterID) {
        ArrayList<Reparation> repairs = new ArrayList<>();

        String sql = "SELECT * FROM Reparations WHERE Scooter="+selectedScooterID+";";

        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();
            while (res.next()) {
                Reparation r = new Reparation();
                r.setScooter(res.getInt("Scooter"));
                r.setUser(res.getInt("User"));
                r.setMechanic(res.getString("Mechanic"));
                r.setComplainTime(res.getString("ComplainTime"));
                r.setRepairTime(res.getString("RepairTime"));
                r.setNote(res.getString("Note"));
                repairs.add(r);
            }
            conn.close();
            if(repairs.size()==0) System.out.println("Aucun trajet effectué");
        } catch (Exception e) {
            System.err.println("Got an exception in getRepairs!");
            System.err.println(e.getMessage());
        }
        return repairs;
    }

    public static ArrayList<Double> getLocalisationScooter(int scooterID){
        ArrayList<Double> pos = new ArrayList<>();
        pos.add(0.0);
        pos.add(0.0);
        String sql;
        if(getLastReload(scooterID) == null && getLastTrip(scooterID) == null){
            pos.add(0.0);
            pos.add(0.0);
            return pos;
        } else if(getLastTrip(scooterID)==null || (getLastReload(scooterID)!=null && getLastReload(scooterID).compareTo(getLastTrip(scooterID))>=0)){
            sql = " SELECT  destinationx,  destinationy FROM Reloads WHERE scooter="+scooterID+" AND endtime = "+
                    "(SELECT MAX(endtime) max FROM Reloads WHERE scooter="+scooterID+");";
        } else {
            sql = "SELECT  destinationx,  destinationy FROM Trips WHERE scooter="+scooterID+" AND endtime = "+
            "(SELECT MAX(endtime) max FROM Trips WHERE scooter="+scooterID+");";

        }
        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();
            if(res.next()) {
                pos.set(0,res.getDouble("destinationx"));
                pos.set(1,res.getDouble("destinationy"));
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in getLocalisationScooter!");
            System.err.println(e.getMessage());
        }
        return pos;

    }
    public static Date getLastReload(int scooterID) {
        String lastReloadString;
        Date lastReload = null;
        String sql = "SELECT MAX(endtime) max FROM Reloads WHERE scooter="+scooterID+";";
        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();
            if(res.next()) {
                lastReloadString = res.getString("max");
                if(lastReloadString != null)
                    lastReload = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(lastReloadString);
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in getLastReload!");
            System.err.println(e.getMessage());
        }
        return lastReload;

    }
    public static Date getLastTrip(int scooterID) {
        String lastTripString ;
        Date lastTrip = null;

        String sql = "SELECT MAX(endtime) max FROM Trips WHERE scooter=" + scooterID + ";";
        Connection conn = getConnection();
        try {
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            ResultSet res = preparedStmt.executeQuery();
            if (res.next()) {
                lastTripString = res.getString("max");
                if(lastTripString != null)
                    lastTrip = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(lastTripString);

            }
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception in getLastTrip!");
            System.err.println(e.getMessage());
        }
        return lastTrip;
    }
    public static void updateLocalisation(int scooterID, Double posX, Double posY){
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "UPDATE Scooters s SET posX ="+posX +
                    ", posY ="+posY+ " WHERE s.sid ="+scooterID+";";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.executeUpdate();
            conn.close();

        } catch (Exception e) {
            System.err.println("Got an exception in updateLocalisation!");
            System.err.println(e.getMessage());
        }
    }
    public static void updateNoLocalisation(int scooterID){
        try {
            Connection conn = DbAccess.getConnection();
            String sql = "UPDATE Scooters s SET posX =NULL, posY = NULL " +
                        "WHERE s.sid ="+scooterID+";";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.executeUpdate();
            conn.close();

        } catch (Exception e) {
            System.err.println("Got an exception in updateNoLocalisation!");
            System.err.println(e.getMessage());
        }
    }
    public static ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();

        // to fill with proper SQL request
        String sql = "SELECT * FROM AnonymeUsers";
        Connection conn = DbAccess.getConnection();
        Statement statement = null;
        try {
            statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            while (res.next()) {
                User user = new User(res.getInt("uid"), res.getString("password"),
                        res.getString("bankaccount"));

                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static Reloader getReloader(int id) {

        int uid = 0;
        String password = "", bankaccount = "", city = "", cp = "", street = "", num = "",
                phone = "", last = "", first = "";
        // to fill with proper SQL request
        String sql = "SELECT * FROM AnonymeUsers WHERE uid="+id;
        Connection conn = DbAccess.getConnection();
        Statement statement = null;
        try {
            statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);

            if (res.next()) {
                uid = res.getInt("uid");
                password = res.getString("password");
                bankaccount = res.getString("bankaccount");
            }
            else throw new SQLException("ID does not exist") ;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        sql = "SELECT * FROM Users WHERE user="+id;

        try {
            statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            if (res.next()) {
                last = res.getString("lastname");
                first = res.getString("firstname");
                phone = res.getString("phone");
                city = res.getString("city");
                cp = res.getString("cp");
                street = res.getString("street");
                num = res.getString("number");
            }
            else throw new SQLException("User is not reloader") ;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new Reloader(uid, last, first, password, phone, city, cp, street, num, bankaccount);
    }


    /* Méthode qui permet d'éviter les duplicats de code */
    private static void createFilteredScooterList(ArrayList<Scooter> scooters, ResultSet res) throws SQLException {
        while (res.next()) {
            Scooter scooter = new Scooter();
            scooter.setId(res.getInt("sid"));
            scooter.setLoad(res.getInt("battery"));
            scooter.setModel(res.getString("model"));
            scooter.setDate(res.getString("dateService"));
            scooter.setPlaint(res.getBoolean("plaint"));
            scooter.setAvailable(res.getInt("available"));
            scooter.setPosX(res.getDouble("posX"));
            scooter.setPosY(res.getDouble("posY"));

            try {
                scooter.setTrips(res.getInt("c"));
            } catch (SQLException e) {
            }
            scooters.add(scooter);
        }
    }

    public static void deleteUser(int id) {
        String sql1 = "DELETE FROM Trips WHERE user ="+id+";";
        String sql2 = "DELETE FROM Reloads WHERE user ="+id+";";
        String sql3 = "DELETE FROM Reparations WHERE User ="+id+";";
        String sql4 = "DELETE FROM Plaints WHERE uid ="+id+";";
        String sql5 = "DELETE FROM Users WHERE user ="+id+";";
        String sql6 = "UPDATE Scooters s SET s.plaint=0 WHERE s.sid IN (SELECT p.sid FROM Plaints p WHERE p.uid="+id+");";
        String sql = "DELETE FROM AnonymeUsers WHERE uid ="+id+";";

        Connection conn = DbAccess.getConnection();
        try {
            PreparedStatement pstmt1 = conn.prepareStatement(sql1);
            PreparedStatement pstmt2 = conn.prepareStatement(sql2);
            PreparedStatement pstmt3 = conn.prepareStatement(sql3);
            PreparedStatement pstmt4 = conn.prepareStatement(sql4);
            PreparedStatement pstmt5 = conn.prepareStatement(sql5);
            PreparedStatement pstmt6 = conn.prepareStatement(sql6);

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt6.executeUpdate();
            pstmt1.executeUpdate();
            pstmt2.executeUpdate();
            pstmt3.executeUpdate();
            pstmt4.executeUpdate();
            pstmt5.executeUpdate();
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.err.println("Got an exception in deleteUser!");
            System.err.println(e.getMessage());
        }
    }

    public static void upgradeUser(int id, String firstname, String surname, String phone, String adNum, String adCity, String adCP, String adStreet) {
        Connection conn = getConnection();

        try {
            String sql = "INSERT INTO Users VALUES(?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setInt(1, id);
            preparedStmt.setString(2, surname);
            preparedStmt.setString(3, firstname);
            preparedStmt.setString(4, phone);
            preparedStmt.setString(5, adCity);
            preparedStmt.setString(6, adCP);
            preparedStmt.setString(7, adStreet);
            preparedStmt.setString(8, adNum);
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Got an exception! Couldn't upgrade user " + Integer.toString(id));
            System.err.println(e.getMessage());
        }
    }

    /* Méthode qui renvoie la trottinette la plus proche d'un endroit donné.
     * Les coordonnées du point doivent être en coordonnées géographiques (latitude et longitude). */
    public static Scooter getClosestScooterFromPoint(Double latitudeP, Double longitudeP) throws SQLException {
        Scooter scooter = new Scooter();
        String sql = "SELECT *, distanceKM(posY,"+latitudeP+",posX,"+longitudeP+") dist " +
                "FROM Scooters " +
                "WHERE available=1 AND distanceKM(posY,"+latitudeP+",posX,"+longitudeP+") IS NOT NULL ORDER BY dist ASC";
        Connection conn = DbAccess.getConnection();
        Statement statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        if(res.next()) {
            scooter.setId(res.getInt("sid"));
            scooter.setLoad(res.getInt("battery"));
            scooter.setModel(res.getString("model"));
            scooter.setDate(res.getString("dateService"));
            scooter.setPlaint(res.getBoolean("plaint"));
            scooter.setPosX(res.getDouble(  "posX"));
            scooter.setPosY(res.getDouble("posY"));
        }
        conn.close();
        return scooter;
    }

    /* Méthode qui renvoie la trottinette la plus proche d'un endroit donné.
     * Les coordonnées du point doivent être en coordonnées géographiques (latitude et longitude). */
    public static ArrayList<Scooter> getClosestScootersFromPoint(Double latitudeP, Double longitudeP, int numScooters) throws SQLException {
        ArrayList<Scooter> scooters = new ArrayList<>();

        String sql = "select s.sid, s.battery, s.dateService, s.model, s.plaint, s.available, s.posX, s.posY, D.dist from scooters s, (\n" +
                "select *, distanceKM(posY,"+latitudeP+",posX,"+longitudeP+") dist from Scooters) D \n" +
                "where s.sid = D.sid and D.dist is not null\n" +
                "order by D.dist\n" +
                "limit " +numScooters;

        String sql2 = "SELECT * FROM Scooters s INNER JOIN (" +
                "SELECT dist FROM (" +
                "SELECT *, distanceKM(posY,"+latitudeP+",posX,"+longitudeP+") dist FROM Scooters ORDER BY dist) D LIMIT "+
                +numScooters+") T ON distanceKM(s.posY,"+latitudeP+",s.posX,"+longitudeP+") = T.dist ORDER BY T.dist;";

        Connection conn = DbAccess.getConnection();
        Statement statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while(res.next()) {
            Scooter scooter = new Scooter();
            scooter.setId(res.getInt("sid"));
            scooter.setLoad(res.getInt("battery"));
            scooter.setModel(res.getString("model"));
            scooter.setDate(res.getString("dateService"));
            scooter.setPlaint(res.getBoolean("plaint"));
            scooter.setPosX(res.getDouble(  "posX"));
            scooter.setPosY(res.getDouble("posY"));
            scooter.setDist(res.getDouble("dist"));
            scooters.add(scooter);
        }
        conn.close();
        return scooters;
    }

    public static ArrayList<Scooter> orderScootersByNumReparations() {
        ArrayList<Scooter> scooters = new ArrayList<>();
        String sql = "SELECT * FROM Scooters s LEFT JOIN (SELECT scooter, COUNT(*) c FROM Reparations "+
                "GROUP BY scooter) t ON t.scooter=s.sid ORDER BY c DESC;";

        try {
            ResultSet res = sendSQLToDb(sql);
            while(res.next()) {
                Scooter scooter = new Scooter();
                scooter.setId(res.getInt("sid"));
                scooter.setLoad(res.getInt("battery"));
                scooter.setModel(res.getString("model"));
                scooter.setDate(res.getString("dateService"));
                scooter.setPlaint(res.getBoolean("plaint"));
                scooter.setPosX(res.getDouble(  "posX"));
                scooter.setPosY(res.getDouble("posY"));
                scooter.setNumReparations(res.getInt("c"));
                scooters.add(scooter);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return scooters;
    }

    public static ResultSet sendSQLToDb(String sql) throws SQLException {
        Connection conn = DbAccess.getConnection();
        Statement statement;
        statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        return res;
    }

    /* Renvoie une ArrayList de data du user. */
    public static ArrayList<User> getUserWithAtLeastTenTripsAndData(){
        ArrayList<User> users = new ArrayList<>();
        String sql = "SELECT * FROM Anonymeusers au,\n" +
                "(SELECT USER, COUNT(*) c, AVG(timespent) t, ROUND(SUM(calculatePrice(timeSpent)), 2) p\n" +
                "FROM\n" +
                "(SELECT USER, TIME_TO_SEC(TIMEDIFF(CAST(endtime AS datetime), CAST(starttime AS datetime))) timeSpent\n" +
                "FROM Trips) AS A\n" +
                "GROUP BY USER) AS U\n" +
                "WHERE U.c >= 10 and au.uid = U.USER";
        try {
            int i=0;
            ResultSet res = sendSQLToDb(sql);
            while(res.next()){
                User user = new User(res.getInt("user"), res.getString("password"), res.getString("bankaccount"));
                user.setR5TripNumber(res.getDouble("c")); // # de trajets
                user.setR5MeanTripLength(res.getDouble("t")); // temps moyen
                user.setR5MoneySpent(res.getDouble("p")); // montant dépensé
                users.add(user);
                i++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static ArrayList<Scooter> getScootersWithAtLeast1150Trips() {

        ArrayList<Scooter> scooters = new ArrayList<>();

        String sql = "select sid, battery, dateService, model, plaint, available, posX, posY, c from Scooters s,\n" +
                "(select t.Scooter, count(*) c from Trips t group by t.Scooter ) a\n" +
                "where a.c > 1200 and s.sid = a.Scooter ORDER BY c DESC;";

        try{
            Connection conn = DbAccess.getConnection();
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery(sql);
            createFilteredScooterList(scooters, res);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return scooters;
    }

    /* Méthode qui renvoie la trottinette qui a effectuée la plus grande distance. */

    public static ArrayList<Scooter> orderScootersByLongestDistance() throws SQLException {
        ArrayList<Scooter> scooters = new ArrayList<>();

        String sql = "SELECT * FROM Scooters s LEFT JOIN\n" +
                "\n" +
                "                  (SELECT scooter, SUM(dist) distCumul\n" +
                "FROM (SELECT *, distanceKM(sourcey, destinationy, sourcex, destinationx) dist\n" +
                "      FROM Trips t) tmp\n" +
                "      GROUP BY tmp.scooter) t\n" +
                "ON t.scooter=s.sid\n" +
                "ORDER BY distCumul DESC;";
        Connection conn = DbAccess.getConnection();
        Statement statement = conn.createStatement();
        ResultSet res = statement.executeQuery(sql);
        while(res.next()) {
            Scooter scooter = new Scooter();
            scooter.setId(res.getInt("sid"));
            scooter.setLoad(res.getInt("battery"));
            scooter.setModel(res.getString("model"));
            scooter.setDate(res.getString("dateService"));
            scooter.setPlaint(res.getBoolean("plaint"));
            scooter.setPosX(res.getDouble(  "posX"));
            scooter.setPosY(res.getDouble("posY"));
            scooter.setDist(res.getDouble("distCumul"));
            scooters.add(scooter);
        }
        return scooters;
    }
}
