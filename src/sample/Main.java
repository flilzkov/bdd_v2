package sample;

import javafx.application.Application;
import javafx.stage.Stage;
import sample.Controller.ViewController;
import sample.Model.*;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;



public class Main extends Application {

    private static boolean dataImportation = false;

    public static void main(String[] args) throws SQLException, FileNotFoundException {

        //Importing data from XML and CSV files
        if(dataImportation) {
            try {
                ArrayList<User> users = DataImporter.readUsersXml();
                DbAccess.insertManyAnonymeUser(users);
                ArrayList<Reloader> reloaders = DataImporter.readReloadersXml();
                DbAccess.insertManyReloaders(reloaders);
                ArrayList<Mechanic> mechanics = DataImporter.readMechanicsXml();
                DbAccess.insertManyMechanics(mechanics);
                ArrayList<Scooter> scooters = DataImporter.readScootersCsv();
                DbAccess.insertManyScooter(scooters);
                ArrayList<Reparation> reparations = DataImporter.readReparationsCsv();
                DbAccess.insertManyReparations(reparations);
                ArrayList<Trip> trips = DataImporter.readTripsCsv();
                DbAccess.insertManyTrips(trips);
                ArrayList<Reload> reloads = DataImporter.readReloadsCsv();
                DbAccess.insertManyReloads(reloads);

                /* Refresh scooter location */
                ArrayList<Double> pos;
                for (Scooter s : scooters){
                    pos = DbAccess.getLocalisationScooter(s.getId());
                    DbAccess.updateLocalisation(s.getId(), pos.get(0), pos.get(1));
                }
                System.out.println("Locations up to date.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        launch(args);

    }

    @Override
    public void start(Stage primaryStage) {

        ViewController.setStage(primaryStage);
        ViewController.goToHomeMenu();
        primaryStage.show();

    }
}
